# Dev Helper
## APT
If you install packages inside the environment using `apt-get`, when you update to
a new version of this image you'll probably need to reinstall those packages.
To avoid that you instead can use something like
`dev-helper apt-install sqlite3 mc`, this will do pretty much the same as
`sudo apt-get install -y sqlite3 mc`, but also save a list so it can automatically
reinstall those packages when a new image version is downloaded.

To remove packages, you can also use `dev-helper apt-remove sqlite3 mc`.

## Bundles
_DevHelper_ provides a set of bundles that let you to install and configure your
environment for a certain language and/or framework.
Visit these links to read more on them:

* [C/C++](bundles/dev-helper.cpp.md)
* [Docker](bundles/dev-helper.docker.md)
* [Golang](bundles/dev-helper.golang.md)
* [NodeJS](bundles/dev-helper.node.md) and frameworks:
    * [Angular](bundles/dev-helper.node.md#angular)
    * [React](bundles/dev-helper.node.md#react)
    * [Vue.js](bundles/dev-helper.node.md#vuejs)
* [PHP](bundles/dev-helper.php.md)
* [Rust](bundles/dev-helper.rust.md)

## DevHelper UI
_VSCoder_ provides you with a simple web page that allows you to check some useful
information about you environment.
Just expose the port `9999` and access it using a web browser.

>If you don't want this service to be started, run your container with the
environment variable `VSCODER_NOUI` set to some value.

## Web Coder
If you don't want to install VSCode in you local machine to remotely access your
_VSCoder_ container, there's a way to access it through a web page with a complete
VSCode interface.

Access through SSH to you container and run `dev-helper enable-web-coder` and it
will install a VSCode server based on
[coder/code-server](https://github.com/coder/code-server), set it as a service and
expose it on port `8888`.
Once it finishes installing, just visit `http://your-container-host:8888/` and
start using it.
If you're prompted to input a password, visit the file
`/home/dev/.config/code-server/config.yaml` inside your container and you'll be
able to obtain and manage that password.

>Warning: In this web based VSCode, compared to a locally installed instance, some
behaviors may vary like extension installations, the use of `code` in the command
line, etc.

### Web Coder Volumes
You may also want to mount some folders/volumes to keep configurations and other
useful assets across image upgrades:

* `/path/to/my/code-server/cache:/home/dev/.local/share/code-server`

### Uninstall
If you don't want it anymore, run `dev-helper disable-web-coder`, this will remove
the service, uninstall related binaries and removed it from memory so it's not
reinstalled on image upgrades.
