# Known Issues
## SSH Issue
When you download a new version of this image, you my end up with a seemingly
security issue looking like this:
```
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@    WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED!     @
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
IT IS POSSIBLE THAT SOMEONE IS DOING SOMETHING NASTY!
Someone could be eavesdropping on you right now (man-in-the-middle attack)!
It is also possible that a host key has just been changed.
The fingerprint for the ECDSA key sent by the remote host is
SHA256:rThDSBz7RlKU2aPy2oYk2xAYF3xi4GksB7f/m8Moqe5.
Please contact your system administrator.
Add correct host key in /home/user/.ssh/known_hosts to get rid of this message.
Offending ECDSA key in /home/user/.ssh/known_hosts:15
  remove with:
  ssh-keygen -f "/home/user/.ssh/known_hosts" -R "[192.168.123.123]:2222"
ECDSA host key for [192.168.123.123]:2222 has changed and you have requested strict checking.
Host key verification failed.
```

If that's your case, don't panic, just run the suggested command and try to
connect again:
```bash
ssh-keygen -f "/home/user/.ssh/known_hosts" -R "[192.168.123.123]:2222"
```
Remember, port and IP my be different for you.

## No Extensions Installed
If you just connect from shell from outside VSCode you may get a message like this
one:
```
VSCode is not installed yet or you're not running this from inside it.
After you remotely access with it, try this command again.
```
Don't worry, just open a terminal inside VSCode and re-run the command and try
again the installation of extension and this time it should work.

## DevHelperUI doesn't update
If you've installed bundles, enabled or disabled features, but the
[_DevHelperUI_](./dev-helper.md#devhelper-ui) is not showing those changes, try
restarting it with this command:
```bash
sudo service dev-helper-ui restart
```
