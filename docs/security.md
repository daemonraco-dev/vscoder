# Security
## Default User
_VSCoder_ gives you a default user called `dev` and it's the one you should use
because many internal behaviors install having it as default.

## Security Modes
_VSCoder_ provides you with three security modes that can be set with the
environment variable `VSCODER_SECURITY` when starting the container:

* `insecure`
* `password`
* `secure`

### 'insecure'
This mode forces the default user `dev` to have the password `dev`.

>This is predictable and really insecure so is not recommended at all.

### 'password'
In this way you can set the user `dev` password by starting the container with the
environment variable `VSCODER_PASSWORD` and your desired password in it.

>This is a bit more secure than `insecure`, but you password may leak and if you
don't change it often, someone may get access to you container.

>If you forget to set this environment variable, the mode will default to
`secure`.

### 'secure'
This is the default mode when the environment variable `VSCODER_SECURITY` is
wrongly set or it's not given.

In this mode, the default user `dev` gets a random password every time the
container is started making it highly impossible to access using with password.
This configuration will force you to work with SSH public keys.

## Working in 'secure' mode
When you are working in mode `secure`, you have a few options to provide your
public SSH key.
In any of those options it is recommended that you mount the user `dev` SSH
configuration folder with something like this (assuming your are using
_docker-compose_):
```yml
version: '3'

services:
  developer:
    image: registry.gitlab.com/daemonraco-dev/vscoder:latest
    volumes:
      - ./ssh-config:/home/dev/.ssh

# ...

```

### Manual Authorization
The simpler way is to manually add your public key to the file `authorized_keys`
inside the SSH configuration folder.

### 'ssh-copy-id'
An alternative when you don't have easy access to copy and paste public key
strings is to start your container as `insecure`, use the command `ssh-copy-id`
from all locations that need access to the container, and then restarting the
container as `secure`.
