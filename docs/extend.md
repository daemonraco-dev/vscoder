# Extend VSCoder
## Why
There are use cases where you like the docker image we provide, but you'd like it
had something in particular.
Let's say you work with some language or framework, you know how to install and
configure it, but it's not provided by default inside the image.

Well, this is where you might follow the Docker way and think of creating your own
image on top of _VSCoder_.

## Your own image
First step is to create your own image. 

Create an empty folder somewhere you like in your computer.
Inside that folder create a file called `Dockerfile` and write this in it:

```dockerfile
FROM registry.gitlab.com/daemonraco-dev/vscoder:latest
```

After you do that, you can run this command inside that folder:
```
docker build . --tag my-vscoder
```

And that's it, now you have in your system an image called `my-vscoder` that is
identical to our image.

## Your own bundle
Following previous examples, let's say you know how to install a specific software
and you want to add that as a bundle.

In the same folder where you have your `Dockerfile`, create another called
`scripts` and inside it create a file called `install-my-bundle.sh`.
Open the new script file in your favorite editor and write something like this:

```sh
#!/bin/bash
. /usr/share/dev-helper/dev-helper.constants.sh;
#
echo -e "\e[31m##################################################################################\e[0m" >&2;
echo -e "\e[31m##################################################################################\e[0m" >&2;
echo -e "\e[31m#                                                                                #\e[0m" >&2;
echo -e "\e[31m#                            MY AWESOME INSTALLER                                #\e[0m" >&2;
echo -e "\e[31m#                                                                                #\e[0m" >&2;
echo -e "\e[31m##################################################################################\e[0m" >&2;
echo -e "\e[31m##################################################################################\e[0m" >&2;
#
# Remembering.
/bin/bash /usr/share/dev-helper/dev-helper.memory.sh install my-bundle;
```
>This is just an example, you need to code your installation steps where all those
`echo` are.

Now create a file called `constants.sh` in the same folder where `Dockerfile` is
and put this inside it.

```sh
#!/bin/bash
#
DEV_HELPER_INSTALLABLES="$DEV_HELPER_INSTALLABLES my-bundle";
```

And the final change is to edit your `Dockerfile` and write something like this in
it:

```dockerfile
FROM registry.gitlab.com/daemonraco-dev/vscoder:latest
#
# Adding helper tools.
COPY ./constants.sh /dev-helper/constants.sh
COPY ./scripts/install-*.sh /usr/share/dev-helper/
```

Now you can rebuild you image and use it as you usually use _VSCoder_.
The big difference is that now you can run `dev-helper install-my-bundle` and it
will run your script, remember it for future image upgrades and appear in the
_DevHelper UI_.
