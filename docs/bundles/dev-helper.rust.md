# Dev Helper & Rust
If you're going to develop using [Rust](https://rust-lang.org/), you can run any
of these commands to install it along with some VSCode extension:

* `dev-helper install-rust`: Installs and sets up Rust utilities.

## Volumes
To make your life easier it's recommended that you set a volume using something
like `/path/to/my/rust/cache:/home/dev/.cargo`.
This will let you keep your downloaded and built Rust packages even if you update
the image.