# Dev Helper & C/C++
If you're going to develop [C/C++](https://www.cplusplus.com/) you can use this
command to install building essential and some extensions for VSCode:
```
dev-helper install-c
```
