# Dev Helper & NodeJS

If you're going to develop using [NodeJS](https://nodejs.org/en/), you can run any
of these commands to install it along with bash autocompletion and some VSCode
extension:

-   `dev-helper install-node`: Just an alias for `dev-helper install-node-21`.
-   `dev-helper install-node-lts`: Just an alias for `dev-helper install-node-20`.
-   `dev-helper install-node-21`: Installs and sets up version 21.x of NodeJS.
-   `dev-helper install-node-20`: Installs and sets up version 20.x of NodeJS.
-   `dev-helper install-node-19`: Installs and sets up version 19.x of NodeJS.
-   `dev-helper install-node-18`: Installs and sets up version 18.x of NodeJS.
-   `dev-helper install-node-17`: Installs and sets up version 17.x of NodeJS.
-   `dev-helper install-node-16`: Installs and sets up version 16.x of NodeJS.

## NPM

If you globally install packages inside the environment using
`npm install --global`, when you update to a new version of this image you'll
probably need to reinstall those packages again.
To avoid that you instead can use something like
`dev-helper npm-install live-server`, this will do pretty much the same as
`sudo npm install --global live-server`, but also save a list so it can
automatically reinstall those packages when a new image version is downloaded.

To remove packages, you can also use `dev-helper npm-remove live-server`.

> _Note_: it require NodeJS to be previously installed.

## Frameworks

### Angular

If you're going to develop using [Angular](https://angular.io/), you can run any
of these commands to globally install `@angular/cli` along with some VSCode
extension:

-   `dev-helper install-angular`: Globally installs the latest _Angular CLI_ and
    VSCode extensions.

> _Note_: it require NodeJS to be previously installed.

### React

If you're going to develop using [React](https://reactjs.org/), you can run any of
these commands to globally install `create-react-app` along with some VSCode
extension:

-   `dev-helper install-react`: Globally installs `create-react-app` and
    VSCode extensions.

> _Note_: it require NodeJS to be previously installed.

### Vue.js

If you're going to develop using [Vue.js](https://vuejs.org/), you can run any of
these commands to globally install the _Vue.js CLI_ along with some VSCode
extension:

-   `dev-helper install-vue`: Globally installs _Vue.js CLI_ and VSCode extensions.

> _Note_: it require NodeJS to be previously installed.
