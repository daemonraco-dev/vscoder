# Dev Helper & Golang

If you're going to develop using [Golang](https://golang.org/), you can run any of
these commands to install it along with bash autocompletion and some VSCode
extension:

-   `dev-helper install-go`: Installs and sets up Golang utilities.

## Volumes

To make your life easier it's recommended that you set a volume using something
like `/path/to/my/go/cache:/home/dev/go`.
This will let you keep your downloaded Go packages even if you update the image.

## go install

If you don't use a particular volume for global packages or if you just want
VSCoder to enforce executables you installed through `go install`, you can use
`dev-helper go-install package.url@version` and VSCoder will keep that in mind for
the next time you update you image.
