# Dev Helper & Docker
If you're going to develop using [Docker](https://www.docker.com/) and/or
`docker-compose`, you can run any of these commands to install it along with some
VSCode extension:

* `dev-helper install-docker`: Configures `apt` and installs `docker` and
`docker-compose`.

## Docker Socket
Because __VSCoder__ is a docker container, installing and running `docker` inside
it is not straight forward.
When you launch this image you need to specify a volume to link the _Docker_
socket on the host machine with the one inside the container.
This will let you run commands seamlessly, but the in essence any command you run
will be performed by the _Docker_ host machine.
Try `sudo docker ps` and you'll even see the image of your __VSCoder__ running.

The volume you need to add is `/var/run/docker.sock:/var/run/docker.sock`,
assuming that you are running a default _Docker_ installation on a Linux system.

__Note__: We suggest [this article](https://devopscube.com/run-docker-in-docker/)
if you want to know more about it (remember that we're not associated with it or
its author).

## Security Risk
If your container gets access to `docker.sock`, it means it has more privileges
over the _Docker_ daemon on the host machine.
Understand the security risks when using it.
