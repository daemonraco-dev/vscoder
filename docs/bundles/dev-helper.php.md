# Dev Helper & PHP
If you're going to develop [PHP](https://www.php.net/) you can use any of these
commands to install it along with some extensions for VSCode:

* `dev-helper install-php-apache`: Installs and sets up version PHP with
[Apache](https://www.apache.org/).
* `dev-helper install-php-nginx`: Installs and sets up version PHP with
[Nginx](https://nginx.org/).
* `dev-helper install-php`: Just an alias for `dev-helper install-php-apache`.

## Volumes
A volume that may be of interest for you is `/home/dev/www` because installing PHP
through `dev-helper` will set it as the default site's root directory. This
applies for both Apache and Nginx installations.
