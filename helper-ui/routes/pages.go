package routes

import (
	"net/http"

	"github.com/go-chi/chi/v5"

	"gitlab.com/daemonraco-dev/dev-helper-ui/handlers"
	"gitlab.com/daemonraco-dev/dev-helper-ui/services"
)

// ___
// Public functions.
func Pages(
	router *chi.Mux,
	config *services.Config,

	docsHandlers handlers.IDocs,
	memoryHandlers handlers.IMemory,
	monitorsHandlers handlers.IMonitors,
	rebuildLogsHandlers handlers.IRebuildLogs,
	systemHandlers handlers.ISystem,
) {
	router.Handle("/", http.RedirectHandler("/memory", http.StatusMovedPermanently))

	router.Get("/logs", rebuildLogsHandlers.Page)
	router.Get("/memory", memoryHandlers.Page)

	if config.CustomMonitorsConfig == nil {
		router.Handle("/monitors", http.RedirectHandler("/memory", http.StatusFound))
	} else {
		router.Get("/monitors", monitorsHandlers.Page)
	}

	router.Get("/system", systemHandlers.Page)

	router.Handle("/help", http.RedirectHandler("/help/README.md", http.StatusMovedPermanently))
	router.Handle("/help/", http.RedirectHandler("/help/README.md", http.StatusMovedPermanently))
	router.Get("/help/*", docsHandlers.Page)
}
