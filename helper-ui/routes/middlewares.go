package routes

import (
	"context"
	"net/http"
	"strings"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"

	"gitlab.com/daemonraco-dev/dev-helper-ui/constants"
	"gitlab.com/daemonraco-dev/dev-helper-ui/types"
)

func RegisterMiddlewares(router *chi.Mux) {
	router.Use(middleware.Logger)
	// router.Use(cacheAssets)
	router.Use(htmxSetContext())
}

func cacheAssets(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if strings.HasPrefix(r.RequestURI, "/assets") {
			w.Header().Set("Cache-Control", "max-age=259200")
		}

		next.ServeHTTP(w, r)
	})
}

func htmxSetContext() func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			values := types.HtmxContext{
				RequestURI: r.RequestURI,
				CurrentUrl: types.HtmxContextUrl{
					Full:        r.Header.Get("Hx-Current-Url"),
					Path:        r.URL.Path,
					QueryParams: r.URL.Query(),
				},
				IsHTMX: r.Header.Get("Hx-Request") == "true",
				Target: r.Header.Get("Hx-Target"),
			}

			ctx := context.WithValue(r.Context(), constants.CtxHtmxContext, values)
			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}
}
