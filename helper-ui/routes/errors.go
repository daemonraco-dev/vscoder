package routes

import (
	"fmt"
	"net/http"

	"github.com/go-chi/chi/v5"

	"gitlab.com/daemonraco-dev/dev-helper-ui/services"
	"gitlab.com/daemonraco-dev/dev-helper-ui/templates/layout"
	"gitlab.com/daemonraco-dev/dev-helper-ui/templates/pages"
)

func RegisterErrorRoutes(router *chi.Mux, config *services.Config) {
	router.NotFound(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusNotFound)
		content := pages.Error(
			"404: Not Found",
			fmt.Sprintf("Page '%s' not found.", r.RequestURI),
		)
		layout.Default(content, "", config).Render(r.Context(), w)
	})
}
