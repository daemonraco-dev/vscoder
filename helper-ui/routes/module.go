package routes

import "go.uber.org/fx"

// ___
// Public constants and variables.
var Module = fx.Module(
	"routes",

	fx.Provide(Router),
	fx.Invoke(RegisterMiddlewares),

	fx.Invoke(Pages),
	fx.Invoke(RegisterErrorRoutes),
	fx.Invoke(RegisterStaticRoutes),
)
