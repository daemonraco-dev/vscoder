package routes

import (
	"fmt"
	"net/http"
	"os"

	"github.com/go-chi/chi/v5"

	"gitlab.com/daemonraco-dev/dev-helper-ui/services"
)

func RegisterStaticRoutes(router *chi.Mux, config *services.Config) error {
	if _, err := os.Stat(config.Assets.Assets); err != nil {
		return fmt.Errorf("directory '%s' does not exists", config.Assets.Assets)
	}

	fs := http.FileServer(http.Dir(config.Assets.Assets))
	router.Handle("/assets/*", http.StripPrefix("/assets/", fs))

	router.Get("/favicon.ico", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Cache-Control", "max-age=259200")
		http.ServeFile(w, r, config.Assets.Assets+"/favicon.ico")
	})

	return nil
}
