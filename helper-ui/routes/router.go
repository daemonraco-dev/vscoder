package routes

import "github.com/go-chi/chi/v5"

// ___
// Public functions.
func Router() *chi.Mux {
    return chi.NewRouter()
}
