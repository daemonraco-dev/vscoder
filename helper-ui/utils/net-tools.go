package utils

import (
	"net"
	"time"
)

// Public
type ScanPortParams struct {
	Hostname string
	Port     string
	Protocol string
}

func ScanPort(params ScanPortParams) (bool, error) {
	if params.Hostname == "" {
		params.Hostname = "127.0.0.1"
	}
	if params.Protocol == "" {
		params.Protocol = "tcp"
	}

	var err error = nil
	var listening bool = false

	address := params.Hostname + ":" + params.Port
	conn, err := net.DialTimeout(params.Protocol, address, 100*time.Millisecond)

	if err == nil {
		listening = true
		defer conn.Close()
	}

	return listening, err
}

func ScanPortAsync(statusChannel chan bool, errorChannel chan error, params ScanPortParams) {
	listening, err := ScanPort(params)

	statusChannel <- listening
	errorChannel <- err
}

//
// Protected
