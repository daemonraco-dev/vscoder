package services

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"os/exec"
	"time"

	"go.uber.org/fx"

	"gitlab.com/daemonraco-dev/dev-helper-ui/utils"
)

// ___
// Public types.
type CustomMonitors struct {
	Active          bool                 `json:"active"`
	Status          CustomMonitorsStatus `json:"status"`
	UpdatedAtFast   time.Time            `json:"updatedAtFast"`
	UpdatedAtMedium time.Time            `json:"updatedAtMedium"`
	UpdatedAtSlow   time.Time            `json:"updatedAtSlow"`

	config *Config
}

type CustomMonitorsStatus struct {
	Customs            []CustomsStatus            `json:"customs"`
	NPMPackageVersions []NPMPackageVersionsStatus `json:"npmPackageVersions"`
	Ports              []PortsStatus              `json:"ports"`
}

type NPMPackageVersionsStatus struct {
	Packages []NPMPackageVersionsStatusPackage `json:"packages"`
	Priority int32                             `json:"priority"`
	Title    string                            `json:"title"`
}

type NPMPackageVersionsStatusPackage struct {
	Label   string `json:"label"`
	Path    string `json:"path"`
	Version string `json:"version"`
	Error   string `json:"error"`
}

type PortsStatus struct {
	Ports    []PortsStatusPort `json:"ports"`
	Priority int32             `json:"priority"`
	Title    string            `json:"title"`
}

type PortsStatusPort struct {
	Label  string `json:"label"`
	Port   string `json:"port"`
	Status bool   `json:"status"`
	Error  string `json:"error"`
}

type CustomsStatus struct {
	ColumnClasses map[string]string `json:"columnClasses"`
	Data          []interface{}     `json:"data"`
	Error         string            `json:"error"`
	Priority      int32             `json:"priority"`
	RefreshSpeed  string            `json:"refreshSpeed"`
	Script        string            `json:"script"`
	Title         string            `json:"title"`
}

type NPMPackage struct {
	Version string `json:"version"`
}

// ___
// Public constants and variables.
var CustomMonitorsModule = fx.Options(
	fx.Provide(func(config *Config) *CustomMonitors {
		monitors := &CustomMonitors{
			config: config,
		}
		monitors.Load()

		return monitors
	}),
)

// ___
// Private types.

// ___
// Private constants and variables.

// ___
// Public functions.
func (cm *CustomMonitors) GetStatus() *CustomMonitors {
	return cm
}

func (cm *CustomMonitors) Load() {
	cm.Active = false
	//
	// Loading configuration file.
	if cm.config.CustomMonitorsConfig != nil {
		log.Printf("")
		log.Printf("Loading custom monitors configuration:")

		if _, err := os.Stat(*cm.config.CustomMonitorsConfig); err == nil {
			fileData, fErr := os.ReadFile(*cm.config.CustomMonitorsConfig)
			if fErr == nil {
				uErr := json.Unmarshal(fileData, &cm.Status)
				if uErr == nil {
					cm.Active = true

					go cm.loadStatusFast()
					go cm.loadStatusMedium()
					go cm.loadStatusSlow()

					log.Printf("\t'%s' loaded.", *cm.config.CustomMonitorsConfig)
				} else {
					log.Printf("\tThe was an error parsing the custom monitors configuration file '%s'.", *cm.config.CustomMonitorsConfig)
					log.Printf("\tError: %s.", uErr.Error())
				}
			} else {
				log.Printf("\tThe was an error loading the file '%s'.", *cm.config.CustomMonitorsConfig)
				log.Printf("\tError: %s.", fErr.Error())
			}
		} else {
			log.Printf("\tThe configuration file '%s' is not present.", *cm.config.CustomMonitorsConfig)
		}
	}
}

// ___
// Private functions.
func (cm *CustomMonitors) loadStatusFast() {
	for {
		cm.rebuildPorts()
		cm.rebuildCustoms("fast")

		cm.UpdatedAtFast = time.Now()
		time.Sleep(2 * time.Second)
	}
}

func (cm *CustomMonitors) loadStatusMedium() {
	for {
		cm.rebuildNPMPackageVersions()
		cm.rebuildCustoms("medium")

		cm.UpdatedAtMedium = time.Now()
		time.Sleep(5 * time.Minute)
	}
}

func (cm *CustomMonitors) loadStatusSlow() {
	for {
		cm.rebuildCustoms("slow")

		cm.UpdatedAtSlow = time.Now()
		time.Sleep(30 * time.Minute)
	}
}

func (cm *CustomMonitors) rebuildCustoms(refreshSpeed string) {
	for customPos, custom := range cm.Status.Customs {
		if custom.RefreshSpeed == refreshSpeed {
			cm.Status.Customs[customPos].Data = nil
			cm.Status.Customs[customPos].Error = ""

			stdout, err := exec.Command(custom.Script).Output()
			if err == nil {
				uErr := json.Unmarshal(stdout, &cm.Status.Customs[customPos].Data)

				if uErr != nil {
					cm.Status.Customs[customPos].Data = make([]interface{}, 0)
					cm.Status.Customs[customPos].Error = uErr.Error()
				}
			} else {
				cm.Status.Customs[customPos].Error = err.Error()
			}
		}
	}
}

func (cm *CustomMonitors) rebuildNPMPackageVersions() {
	for packSetPos, packSet := range cm.Status.NPMPackageVersions {
		for packPos, pack := range packSet.Packages {
			cm.Status.NPMPackageVersions[packSetPos].Packages[packPos].Version = ""
			cm.Status.NPMPackageVersions[packSetPos].Packages[packPos].Error = ""

			if _, err := os.Stat(pack.Path); err == nil {
				fileData, fErr := os.ReadFile(pack.Path)
				if fErr == nil {
					data := NPMPackage{Version: "unknown"}
					uErr := json.Unmarshal(fileData, &data)
					if uErr == nil {
						cm.Status.NPMPackageVersions[packSetPos].Packages[packPos].Version = data.Version
					} else {
						cm.Status.NPMPackageVersions[packSetPos].Packages[packPos].Error = uErr.Error()

						log.Printf("\tThe was an error parsing '%s'.", pack.Path)
						log.Printf("\tError: %s.", uErr.Error())
					}
				} else {
					cm.Status.NPMPackageVersions[packSetPos].Packages[packPos].Error = fErr.Error()

					log.Printf("\tThe was an error loading the file '%s'.", pack.Path)
					log.Printf("\tError: %s.", fErr.Error())
				}
			} else {
				cm.Status.NPMPackageVersions[packSetPos].Packages[packPos].Error =
					fmt.Sprintf("\tThe configuration file '%s' is not present.", pack.Path)
				log.Printf("\tThe configuration file '%s' is not present.", pack.Path)
			}
		}
	}
}

func (cm *CustomMonitors) rebuildPorts() {
	statusChannel := make(chan bool)
	errorChannel := make(chan error)

	for portSetPos, portSet := range cm.Status.Ports {
		for portPos, port := range portSet.Ports {
			cm.Status.Ports[portSetPos].Ports[portPos].Status = false
			cm.Status.Ports[portSetPos].Ports[portPos].Error = ""

			go utils.ScanPortAsync(statusChannel, errorChannel, utils.ScanPortParams{Port: port.Port})
			status := <-statusChannel
			pErr := <-errorChannel
			if pErr == nil {
				cm.Status.Ports[portSetPos].Ports[portPos].Status = status
			} else {
				cm.Status.Ports[portSetPos].Ports[portPos].Error = pErr.Error()
			}
		}
	}
}
