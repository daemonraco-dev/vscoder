package services

import (
	"log"
	"os"
	"strings"

	"go.uber.org/fx"
)

// ___
// Public types.
type MemoryStatus struct {
	AptPacks []string            `json:"aptPacks"`
	GoPacks  []string            `json:"goPacks"`
	NPMPacks []string            `json:"npmPacks"`
	Rebuild  MemoryStatusRebuild `json:"rebuild"`
}

type MemoryStatusRebuild struct {
	Bundles  MemoryStatusRebuildBundles  `json:"bundles"`
	Features MemoryStatusRebuildFeatures `json:"features"`
}

type MemoryStatusRebuildBundles struct {
	Alias     map[string]string `json:"alias"`
	Available []string          `json:"available"`
	Installed []string          `json:"installed"`
}

type MemoryStatusRebuildFeatures struct {
	Available []string `json:"available"`
	Installed []string `json:"installed"`
}

// ___
// Public constants and variables.
var MemoryStatusModule = fx.Options(
	fx.Provide(func() *MemoryStatus {
		ms := &MemoryStatus{}

		ms.AptPacks = []string{}
		ms.GoPacks = []string{}
		ms.NPMPacks = []string{}

		ms.Rebuild = MemoryStatusRebuild{}

		ms.Rebuild.Bundles = MemoryStatusRebuildBundles{}
		ms.Rebuild.Bundles.Alias = make(map[string]string)
		ms.Rebuild.Bundles.Available = []string{}
		ms.Rebuild.Bundles.Installed = []string{}

		if os.Getenv("DEV_HELPER_INSTALLABLES") != "" {
			ms.Rebuild.Bundles.Available = strings.Split(os.Getenv("DEV_HELPER_INSTALLABLES"), " ")
		}
		if os.Getenv("DEV_HELPER_INSTALLABLES_ALIAS") != "" {
			for _, alias := range strings.Split(os.Getenv("DEV_HELPER_INSTALLABLES_ALIAS"), " ") {
				pieces := strings.Split(alias, ":")
				ms.Rebuild.Bundles.Alias[pieces[0]] = pieces[1]
			}
		}

		ms.Rebuild.Features = MemoryStatusRebuildFeatures{}
		ms.Rebuild.Features.Available = []string{}
		ms.Rebuild.Features.Installed = []string{}

		if os.Getenv("DEV_HELPER_FEATURES") != "" {
			ms.Rebuild.Features.Available = strings.Split(os.Getenv("DEV_HELPER_FEATURES"), " ")
		}

		return ms
	}),
)

// ___
// Public functions.
func (ms *MemoryStatus) Update() {
	ms.AptPacks = readMemoryFile("/dev-helper/memory/apt-packs")
	ms.GoPacks = readMemoryFile("/dev-helper/memory/go-packs")
	ms.NPMPacks = readMemoryFile("/dev-helper/memory/npm-packs")
	readRebuildMemory(&ms.Rebuild)
}

// ___
// Private functions.
func readMemoryFile(memoryFile string) []string {
	result := []string{}

	if _, err := os.Stat(memoryFile); err == nil {
		result = []string{}

		data, fErr := os.ReadFile(memoryFile)
		if fErr == nil {
			for _, line := range strings.Split(string(data), "\n") {
				if line != "" {
					result = append(result, line)
				}
			}
		} else {
			log.Printf("Unable to read memory file '%s'.", memoryFile)
		}
	} else {
		log.Printf("Memory file '%s' does not exists.", memoryFile)
	}

	return result
}

func readRebuildMemory(status *MemoryStatusRebuild) {
	status.Bundles.Installed = []string{}

	memoryFile := "/dev-helper/memory/rebuild.sh"
	bundlePrefix := "dev-helper install-"
	featurePrefix := "dev-helper enable-"

	if _, err := os.Stat(memoryFile); err == nil {
		data, fErr := os.ReadFile(memoryFile)
		if fErr == nil {
			for _, line := range strings.Split(string(data), "\n") {
				if strings.HasPrefix(line, bundlePrefix) {
					status.Bundles.Installed = append(status.Bundles.Installed, strings.ReplaceAll(strings.TrimPrefix(line, bundlePrefix), ";", ""))
				} else if strings.HasPrefix(line, featurePrefix) {
					status.Features.Installed = append(status.Features.Installed, strings.ReplaceAll(strings.TrimPrefix(line, featurePrefix), ";", ""))
				}
			}
		} else {
			log.Printf("Unable to read memory file '%s'.", memoryFile)
		}
	} else {
		log.Printf("Memory file '%s' does not exists.", memoryFile)
	}
}
