package services

import (
	"sort"

	shirouHost "github.com/shirou/gopsutil/host"
	shirouLoad "github.com/shirou/gopsutil/load"
	shirouProcess "github.com/shirou/gopsutil/process"
	"go.uber.org/fx"
)

// ___
// Public types.
type SystemStatus struct {
	AvgStats  *shirouLoad.AvgStat   `json:"avgStats"`
	MiscStats *shirouLoad.MiscStat  `json:"miscStats"`
	Processes []SystemStatusProcess `json:"processes"`
	Stats     *shirouHost.InfoStat  `json:"stats"`
}

type SystemStatusProcess struct {
	CPUPercent    float64                       `json:"cpuPercent"`
	Executable    string                        `json:"executable"`
	MemoryInfo    *shirouProcess.MemoryInfoStat `json:"memoryInfo"`
	MemoryPercent float32                       `json:"memoryPercent"`
	PID           int32                         `json:"pid"`
	PPID          int32                         `json:"ppid"`
	Status        string                        `json:"status"`
	Username      string                        `json:"username"`
}

// ___
// Public constants and variables.
var SystemStatusModule = fx.Options(
	fx.Provide(func() *SystemStatus {
		out := new(SystemStatus)

		out.MiscStats = nil
		out.Processes = []SystemStatusProcess{}
		out.Stats = nil

		return out
	}),
)

// ___
// Public functions.
func (status *SystemStatus) Update(order string) {
	status.AvgStats, _ = shirouLoad.Avg()
	status.MiscStats, _ = shirouLoad.Misc()
	status.Stats, _ = shirouHost.Info()

	processList, _ := shirouProcess.Processes()
	status.Processes = []SystemStatusProcess{}
	for i := range processList {
		cmdline, _ := processList[i].Cmdline()
		cpuPercent, _ := processList[i].CPUPercent()
		memoryInfo, _ := processList[i].MemoryInfo()
		ppid, _ := processList[i].Ppid()
		pStatus, _ := processList[i].Status()
		username, _ := processList[i].Username()
		memoryPercent, _ := processList[i].MemoryPercent()

		status.Processes = append(status.Processes, SystemStatusProcess{
			CPUPercent:    cpuPercent,
			Executable:    cmdline,
			MemoryPercent: memoryPercent,
			MemoryInfo:    memoryInfo,
			PID:           processList[i].Pid,
			PPID:          ppid,
			Status:        pStatus,
			Username:      username,
		})
	}

	switch order {
	case "cpu", "default":
		sort.SliceStable(status.Processes, func(i, j int) bool {
			return status.Processes[i].CPUPercent > status.Processes[j].CPUPercent
		})
	case "executable":
		sort.SliceStable(status.Processes, func(i, j int) bool {
			return status.Processes[i].Executable < status.Processes[j].Executable
		})
	case "memory":
		sort.SliceStable(status.Processes, func(i, j int) bool {
			return status.Processes[i].MemoryInfo.RSS > status.Processes[j].MemoryInfo.RSS
		})
	case "memory-percentage":
		sort.SliceStable(status.Processes, func(i, j int) bool {
			return status.Processes[i].MemoryPercent > status.Processes[j].MemoryPercent
		})
	}
}
