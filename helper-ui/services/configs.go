package services

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"os/exec"
	"path"
	"strings"

	"go.uber.org/fx"
)

// ___
// Public types.
type Config struct {
	Assets               ConfigAssets `json:"assets"`
	CustomMonitorsConfig *string      `json:"customMonitorsConfig"`
	Distro               string       `json:"distro"`
	Port                 string       `json:"port"`
}

type ConfigAssets struct {
	Assets string `json:"assets"`
	Docs   string `json:"docs"`
}

// ___
// Public constants and variables.
var Configuration = fx.Options(
	fx.Provide(func() (*Config, error) {
		config := &Config{}
		return config, config.load()
	}),
)

// ___
// Private functions.
func (c *Config) load() error {
	//
	// Setting up logs
	log.SetPrefix(">> ")
	log.SetFlags(0)

	log.Printf("Loading configurations...")
	//
	// Guessing the configuration file.
	configPath := "/dev-helper/ui/config.json"
	if os.Getenv("CONFIG") != "" {
		configPath = os.Getenv("CONFIG")
	}
	//
	// Loading configuration file.
	if _, err := os.Stat(configPath); err == nil {
		data, fErr := os.ReadFile(configPath)
		if fErr == nil {
			uErr := json.Unmarshal(data, &c)
			if uErr != nil {
				return fmt.Errorf("unable to read configuration file '%s'", configPath)
			}
		} else {
			return fmt.Errorf("unable to read configuration file '%s'", configPath)
		}
	} else {
		return fmt.Errorf("configuration file '%s' does not exists", configPath)
	}
	//
	// Loading port configuration.
	if os.Getenv("PORT") != "" {
		c.Port = os.Getenv("PORT")
	}
	//
	// Setting current public assets directory.
	if os.Getenv("ASSETS") != "" {
		c.Assets.Assets = os.Getenv("ASSETS")
	} else if c.Assets.Assets == "" {
		bin, _ := os.Executable()
		c.Assets.Assets = path.Join(path.Dir(bin), "public")
	}
	//
	// Setting current docs directory.
	if os.Getenv("DOCS") != "" {
		c.Assets.Docs = os.Getenv("DOCS")
	} else if c.Assets.Docs == "" {
		bin, _ := os.Executable()
		c.Assets.Docs = path.Join(path.Dir(bin), "docs")
	}
	//
	// Loading distro's name.
	cmd := exec.Command("lsb_release", "--codename")
	output, err := cmd.Output()
	if err != nil {
		return err
	}
	c.Distro = strings.TrimSpace(strings.Split(string(output), ":")[1])
	//
	// Logging configuration.
	log.Printf("Configuration:\n")
	log.Printf("  Path:   '%s'\n", configPath)
	log.Printf("  Distro: '%s'\n", c.Distro)
	log.Printf("  Port:   ':%s'\n", c.Port)
	log.Printf("  Assets:\n")
	log.Printf("    Assets: '%s'\n", c.Assets.Assets)
	log.Printf("    Docs:   '%s'\n", c.Assets.Docs)

	return nil
}
