package services

import "go.uber.org/fx"

// ___
// Public constants and variables.
var Module = fx.Module(
	"services",

	Configuration,
	CustomMonitorsModule,
	MemoryStatusModule,
	SystemStatusModule,
)
