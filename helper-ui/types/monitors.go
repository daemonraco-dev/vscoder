package types

// ___
// Public types.
type MonitorsGenericPack struct {
	Data     interface{}
	Priority int32
	Type     string
}

// ___
// Public constants and variables.

// ___
// Private types.

// ___
// Private constants and variables.

// ___
// Public functions.

// ___
// Private functions.
