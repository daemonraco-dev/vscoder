package types

// ___
// Public types.
type RebuildLog struct {
	Cached  bool   `json:"cached"`
	Log     string `json:"log"`
	Running bool   `json:"running"`
}
