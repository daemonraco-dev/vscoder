package types

import "net/url"

// ___
// Public types.
type HtmxContext struct {
	CurrentUrl HtmxContextUrl
	IsHTMX     bool
	Managers   HtmxContextManagers
	RequestURI string
	Target     string
}

type HtmxContextUrl struct {
	Full        string
	Path        string
	QueryParams url.Values
}

type HtmxContextManagers struct {
	Hooks     interface{}
	MenuTools interface{}
}

// ___
// Public functions.
func (u HtmxContextUrl) WithParams(override url.Values) HtmxContextUrl {
	newUrl := HtmxContextUrl{
		Full:        u.Full,
		Path:        u.Path,
		QueryParams: url.Values{},
	}

	for k, v := range u.QueryParams {
		newUrl.QueryParams[k] = v
	}
	for k, v := range override {
		newUrl.QueryParams[k] = v
	}

	return newUrl
}

func (u HtmxContextUrl) ToString() string {
	return u.Path + "?" + u.QueryParams.Encode()
}
