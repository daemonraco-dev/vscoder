module gitlab.com/daemonraco-dev/dev-helper-ui

go 1.21.5

require (
	github.com/a-h/templ v0.2.513
	github.com/dustin/go-humanize v1.0.1
	github.com/go-chi/chi/v5 v5.0.11
	github.com/gomarkdown/markdown v0.0.0-20211203165214-0d698b49fbb4
	github.com/shirou/gopsutil v3.21.11+incompatible
	go.uber.org/fx v1.20.1
)

require (
	github.com/go-ole/go-ole v1.2.6 // indirect
	github.com/tklauser/go-sysconf v0.3.9 // indirect
	github.com/tklauser/numcpus v0.3.0 // indirect
	github.com/yusufpapurcu/wmi v1.2.2 // indirect
	go.uber.org/atomic v1.10.0 // indirect
	go.uber.org/dig v1.17.0 // indirect
	go.uber.org/multierr v1.9.0 // indirect
	go.uber.org/zap v1.24.0 // indirect
	golang.org/x/sys v0.14.0 // indirect
)
