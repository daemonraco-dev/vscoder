package main

import (
	"context"
	"log"
	"net/http"

	"github.com/go-chi/chi/v5"
	"go.uber.org/fx"

	"gitlab.com/daemonraco-dev/dev-helper-ui/handlers"
	"gitlab.com/daemonraco-dev/dev-helper-ui/routes"
	"gitlab.com/daemonraco-dev/dev-helper-ui/services"
)

// ___
// Public functions.
func main() {
	app := fx.New(
		handlers.Module,
		services.Module,
		routes.Module,

		fx.Invoke(serve),
		fx.NopLogger,
	)

	if app.Err() != nil {
		panic(app.Err())
	} else {
		app.Run()
	}
}

// ___
// Private functions.
func serve(lifecycle fx.Lifecycle, router *chi.Mux, config *services.Config) {
	lifecycle.Append(fx.Hook{
		OnStart: func(ctx context.Context) error {
			log.Print()
			log.Printf("Listening on port %s", config.Port)
			go http.ListenAndServe(":"+config.Port, router)

			return nil
		},
	})
}
