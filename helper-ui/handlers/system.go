package handlers

import (
	"net/http"

	"go.uber.org/fx"

	"gitlab.com/daemonraco-dev/dev-helper-ui/services"
	"gitlab.com/daemonraco-dev/dev-helper-ui/templates/layout"
	"gitlab.com/daemonraco-dev/dev-helper-ui/templates/pages"
)

// ___
// Public types.
type ISystem interface {
	Page(w http.ResponseWriter, r *http.Request)
}

type System struct {
	config       *services.Config
	systemStatus *services.SystemStatus
}

// ___
// Public constants and variables.
var SystemModule = fx.Options(
	fx.Provide(func(
		config *services.Config,
		systemStatus *services.SystemStatus,
	) ISystem {
		return &System{
			config:       config,
			systemStatus: systemStatus,
		}
	}),
)

// ___
// Public functions.
func (h System) Page(w http.ResponseWriter, r *http.Request) {
	order := r.URL.Query().Get("order")
	if order == "" {
		order = "default"
	}

	h.systemStatus.Update(order)

	content := pages.System(h.systemStatus, order)
	layout.Default(content, "system", h.config).Render(r.Context(), w)
}
