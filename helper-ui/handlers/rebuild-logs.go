package handlers

import (
	"net/http"
	"os"
	"time"

	"go.uber.org/fx"

	"gitlab.com/daemonraco-dev/dev-helper-ui/services"
	"gitlab.com/daemonraco-dev/dev-helper-ui/templates/layout"
	"gitlab.com/daemonraco-dev/dev-helper-ui/templates/pages"
	"gitlab.com/daemonraco-dev/dev-helper-ui/types"
)

// ___
// Public types.
type IRebuildLogs interface {
	Page(w http.ResponseWriter, r *http.Request)
}

type RebuildLogs struct {
	config *services.Config
}

// ___
// Public constants and variables.
var RebuildLogsModule = fx.Options(
	fx.Provide(func(config *services.Config) IRebuildLogs {
		return &RebuildLogs{
			config: config,
		}
	}),
)

// ___
// Private constants and variables.
var rebuildLogCache string
var rebuildLogCacheDate time.Time

// ___
// Public functions.
func (h RebuildLogs) Page(w http.ResponseWriter, r *http.Request) {
	content := pages.RebuildLogs(getRebuildLog())
	layout.Default(content, "rebuild-logs", h.config).Render(r.Context(), w)
}

// ___
// Private functions.
func getRebuildLog() types.RebuildLog {
	result := types.RebuildLog{
		Cached:  false,
		Log:     "",
		Running: false,
	}

	if time.Since(rebuildLogCacheDate) > time.Minute {
		rebuildLogCache = ""
		rebuildLogCacheDate = time.Now()
	}

	filePath := "/tmp/dev-helper.rebuild-memory.log"
	if _, err := os.Stat(filePath); err != nil {
		filePath = "/tmp/dev-helper.rebuild-memory.done"
	} else {
		result.Running = true
	}

	if _, err := os.Stat(filePath); err == nil {
		if !result.Running && rebuildLogCache != "" {
			result.Log = rebuildLogCache
			result.Cached = true
		} else {
			rebuildLogCache = ""

			data, fErr := os.ReadFile(filePath)
			if fErr == nil {
				result.Log = string(data)
				rebuildLogCache = result.Log
			} else {
				result.Log = "Unable to obtain read the rebuild log"
			}
		}
	} else {
		result.Log = "Unable to obtain the rebuild log"
	}

	return result
}
