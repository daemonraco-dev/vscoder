package handlers

import "go.uber.org/fx"

// ___
// Public constants and variables.
var Module = fx.Module(
	"handlers",

	DocsModule,
	MemoryModule,
	MonitorsModule,
	RebuildLogsModule,
	SystemModule,
)
