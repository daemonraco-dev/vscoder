package handlers

import (
	"net/http"

	"go.uber.org/fx"

	"gitlab.com/daemonraco-dev/dev-helper-ui/services"
	"gitlab.com/daemonraco-dev/dev-helper-ui/templates/layout"
	"gitlab.com/daemonraco-dev/dev-helper-ui/templates/pages"
)

// ___
// Public types.
type IMemory interface {
	Page(w http.ResponseWriter, r *http.Request)
}

type Memory struct {
	config       *services.Config
	memoryStatus *services.MemoryStatus
}

// ___
// Public constants and variables.
var MemoryModule = fx.Options(
	fx.Provide(func(
		config *services.Config,
		memoryStatus *services.MemoryStatus,
	) IMemory {
		return &Memory{
			config:       config,
			memoryStatus: memoryStatus,
		}
	}),
)

// ___
// Public functions.
func (h Memory) Page(w http.ResponseWriter, r *http.Request) {
	h.memoryStatus.Update()

	content := pages.Memory(h.memoryStatus, h.config)
	layout.Default(content, "memory", h.config).Render(r.Context(), w)
}
