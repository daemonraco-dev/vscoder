package handlers

import (
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"github.com/gomarkdown/markdown"
	"github.com/gomarkdown/markdown/html"
	"github.com/gomarkdown/markdown/parser"
	"go.uber.org/fx"

	"gitlab.com/daemonraco-dev/dev-helper-ui/services"
	"gitlab.com/daemonraco-dev/dev-helper-ui/templates/layout"
	"gitlab.com/daemonraco-dev/dev-helper-ui/templates/pages"
)

// ___
// Public types.
type IDocs interface {
	Page(w http.ResponseWriter, r *http.Request)
}

type Docs struct {
	config *services.Config
}

// ___
// Public constants and variables.
var DocsModule = fx.Options(
	fx.Provide(func(config *services.Config) IDocs {
		return &Docs{
			config: config,
		}
	}),
)

// ___
// Public functions.
func (h Docs) Page(w http.ResponseWriter, r *http.Request) {
	docPath, err := filepath.Abs(h.config.Assets.Docs + "/" + strings.TrimPrefix(r.RequestURI, "/help/"))
	if err != nil || !strings.HasPrefix(docPath, h.config.Assets.Docs) {
		w.WriteHeader(http.StatusBadRequest)
		log.Print(err)
	} else {
		//
		// Loading document.
		md, err := os.ReadFile(docPath)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			log.Print(err)
		}
		//
		// Create markdown parser with extensions.
		extensions := parser.CommonExtensions | parser.AutoHeadingIDs | parser.Tables | parser.FencedCode | parser.OrderedListStart
		p := parser.NewWithExtensions(extensions)
		doc := p.Parse(md)
		//
		// Create HTML renderer with extensions
		htmlFlags := html.CommonFlags | html.HrefTargetBlank
		opts := html.RendererOptions{Flags: htmlFlags}
		renderer := html.NewRenderer(opts)

		w.Header().Set("Cache-Control", "public, max-age=259200") // 3d

		content := pages.Doc(string(markdown.Render(doc, renderer)))
		layout.Default(content, "docs", h.config).Render(r.Context(), w)
	}
}
