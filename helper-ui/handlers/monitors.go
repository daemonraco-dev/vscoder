package handlers

import (
	"net/http"
	"slices"

	"go.uber.org/fx"

	"gitlab.com/daemonraco-dev/dev-helper-ui/services"
	"gitlab.com/daemonraco-dev/dev-helper-ui/templates/layout"
	"gitlab.com/daemonraco-dev/dev-helper-ui/templates/pages"
	"gitlab.com/daemonraco-dev/dev-helper-ui/types"
)

// ___
// Public types.
type IMonitors interface {
	Page(w http.ResponseWriter, r *http.Request)
}

type Monitors struct {
	config         *services.Config
	customMonitors *services.CustomMonitors
}

// ___
// Public constants and variables.
var MonitorsModule = fx.Options(
	fx.Provide(func(
		config *services.Config,
		customMonitors *services.CustomMonitors,
	) IMonitors {
		return &Monitors{
			config:         config,
			customMonitors: customMonitors,
		}
	}),
)

// ___
// Public functions.
func (h Monitors) Page(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Cache-Control", "public, max-age=2") // 2s

	rawStatus := h.customMonitors.GetStatus()
	status := []*types.MonitorsGenericPack{}
	for _, pack := range rawStatus.Status.NPMPackageVersions {
		status = append(status, &types.MonitorsGenericPack{
			Data:     pack,
			Priority: pack.Priority,
			Type:     "npm-package",
		})
	}
	for _, pack := range rawStatus.Status.Ports {
		status = append(status, &types.MonitorsGenericPack{
			Data:     pack,
			Priority: pack.Priority,
			Type:     "port",
		})
	}
	for _, pack := range rawStatus.Status.Customs {
		status = append(status, &types.MonitorsGenericPack{
			Data:     pack,
			Priority: pack.Priority,
			Type:     "custom",
		})
	}
	slices.SortFunc(status, func(a *types.MonitorsGenericPack, b *types.MonitorsGenericPack) int {
		return int(a.Priority - b.Priority)
	})

	left := []*types.MonitorsGenericPack{}
	right := []*types.MonitorsGenericPack{}
	isLeft := true
	for _, entry := range status {
		if isLeft {
			left = append(left, entry)
		} else {
			right = append(right, entry)
		}

		isLeft = !isLeft
	}

	content := pages.Monitors(rawStatus, left, right)
	layout.Default(content, "monitors", h.config).Render(r.Context(), w)
}

// ___
// Private functions.
