#!/bin/bash
. /usr/share/dev-helper/dev-helper.constants.sh;
#
DEV_HELPER_RECALL_LOG=/tmp/dev-helper.rebuild-memory.log;
#
DEV_HELPER_RECALL_DONE=${DEV_HELPER_RECALL_LOG%.log}.done;
#
if [ -e $DEV_HELPER_RECALL_LOG ]; then
    clear;
    echo -e "\e[33mTailing '$DEV_HELPER_RECALL_LOG'...\e[0m\n";
    tail -f $DEV_HELPER_RECALL_LOG;
elif [ -e $DEV_HELPER_RECALL_DONE ]; then
    clear;
    echo -e "\e[33mDumping '$DEV_HELPER_RECALL_DONE':\e[0m\n";
    cat $DEV_HELPER_RECALL_DONE;
else
    echo -e "\e[33mAlready recalling.\e[0m" >&2;
fi;
