#!/bin/bash
. /usr/share/dev-helper/dev-helper.constants.sh;
#
if [ -n "$(which node)" ]; then
    sudo npm install --global @vue/cli;
    #
    /bin/bash /usr/share/dev-helper/install-default-extensions.sh;
    /bin/bash /usr/share/dev-helper/install-vue-extensions.sh;
    #
    # Remembering
    /bin/bash /usr/share/dev-helper/dev-helper.memory.sh install vue;
else
    echo -e "\e[31mNodeJS is not intalled yet.\e[0m" >&2;
    echo -e "\e[31mTry running 'dev-helper install-node' first.\e[0m" >&2;
fi;
