#!/bin/bash
. /usr/share/dev-helper/dev-helper.constants.sh;
#
if [ -z "$(which g++)" ]; then
    sudo apt-get update;
    sudo apt-get install -y build-essential;
fi;
