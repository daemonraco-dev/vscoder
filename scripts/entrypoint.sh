#!/bin/bash
#
. /usr/share/dev-helper/dev-helper.constants.sh;
#
# -< Reinforncing global assets >----------------------------------------------- #
#
# Enforcing 'dev-helper' directories.
if [ ! -e $DEV_HELPER_MEMORY_DIR ]; then
    mkdir -vp $DEV_HELPER_MEMORY_DIR;
fi;
#
# Enforcing 'dev-helper-ui' service.
if [ -z "$VSCODER_NOUI" ]; then
    sudo service dev-helper-ui start;
else
    sudo service dev-helper-ui stop;
fi;
#
# -< Reinforcing global assets >------------------------------------------------ #
#
# Generating a global and memorized bash configuration.
if [ ! -e /dev-helper/memory/bashrc ]; then
        cat > /dev-helper/memory/bashrc << __ENDL__
#!/bin/bash
#
__ENDL__
fi;
if [ -z "$(cat /etc/bash.bashrc | grep '/dev-helper/memory/bashrc')" ]; then
    echo ""                                                  | sudo tee -a /etc/bash.bashrc >/dev/null;
    echo "# DevHelper memorized global Bash configurations." | sudo tee -a /etc/bash.bashrc >/dev/null;
    echo ". /dev-helper/memory/bashrc;"                      | sudo tee -a /etc/bash.bashrc >/dev/null;
fi;
#
# -< Reinforcing local assets >------------------------------------------------- #
#
# Enforcing and fixing '$HOME/.ssh';
if [ ! -e $DEV_HELPER_HOME_DIR/.ssh ]; then
    mkdir -vp $DEV_HELPER_HOME_DIR/.ssh;
fi;
sudo chown -v $DEV_HELPER_PUBLIC_OWNER $DEV_HELPER_HOME_DIR/.ssh 2>&1 | grep -v retained;
#
# Enforcing and fixing '$HOME/.vscode-server';
if [ ! -e $DEV_HELPER_HOME_DIR/.vscode-server ]; then
    mkdir -vp $DEV_HELPER_HOME_DIR/.vscode-server;
fi;
sudo chown -v $DEV_HELPER_PUBLIC_OWNER $DEV_HELPER_HOME_DIR/.vscode-server 2>&1 | grep -v retained;
#
# Enforcing and fixing '$HOME/.dev-helper';
if [ ! -e $DEV_HELPER_HOME_DIR/.dev-helper ]; then
    mkdir -vp $DEV_HELPER_HOME_DIR/.dev-helper;
fi;
sudo chown -vR $DEV_HELPER_PUBLIC_OWNER $DEV_HELPER_HOME_DIR/.dev-helper 2>&1 | grep -v retained;
#
# Enforcing and fixing '$HOME/.gitconfig';
if [ ! -e $DEV_HELPER_HOME_DIR/.dev-helper/gitconfig ]; then
    touch $DEV_HELPER_HOME_DIR/.dev-helper/gitconfig;
fi;
if [ ! -e $DEV_HELPER_HOME_DIR/.gitconfig ]; then
    ln -s $DEV_HELPER_HOME_DIR/.dev-helper/gitconfig $DEV_HELPER_HOME_DIR/.gitconfig;
fi;
#
# Enforcing and fixing '$HOME/.config';
mkdir -p $DEV_HELPER_HOME_DIR/.dev-helper/config;
if [ ! -e $DEV_HELPER_HOME_DIR/.config ]; then
    ln -s $DEV_HELPER_HOME_DIR/.dev-helper/config $DEV_HELPER_HOME_DIR/.config;
fi;
sudo chown -vR $DEV_HELPER_PUBLIC_OWNER $DEV_HELPER_HOME_DIR/.dev-helper/config 2>&1 | grep -v retained;
#
# Connecting '$HOME/.bashrc' to a personal one.
if [ ! -e $DEV_HELPER_HOME_DIR/.dev-helper/bashrc ]; then
        cat > $DEV_HELPER_HOME_DIR/.dev-helper/bashrc << __ENDL__
#!/bin/bash
#
# Here are some tools that may be useful for you, just uncomment the ones you want
# to use:
#. /usr/share/dev-helper/bash-tools/git-prompt.sh
#. /usr/share/dev-helper/bash-tools/git-aliases.sh
#. /usr/share/dev-helper/bash-tools/git-ast.sh
#. /usr/share/dev-helper/bash-tools/command-runner.sh
#. /usr/share/dev-helper/bash-tools/other-aliases.sh
__ENDL__
fi;
sudo chown -v $DEV_HELPER_PUBLIC_OWNER $DEV_HELPER_HOME_DIR/.dev-helper/bashrc;
if [ -z "$(grep "$DEV_HELPER_HOME_DIR/.dev-helper/bashrc" $DEV_HELPER_HOME_DIR/.bashrc)" ]; then
    echo ". $DEV_HELPER_HOME_DIR/.dev-helper/bashrc;" >> $DEV_HELPER_HOME_DIR/.bashrc;
fi;
#
# Adding a local 'bin' directory to '$PATH'.
if [ -z "$(grep "$DEV_HELPER_HOME_DIR/.dev-helper/bin" $DEV_HELPER_HOME_DIR/.bashrc)" ]; then
    echo "export PATH=\"\$PATH:$DEV_HELPER_HOME_DIR/.dev-helper/bin\";" >> $DEV_HELPER_HOME_DIR/.bashrc;
fi;
#
# Connecting '$HOME/.bash_history' to a more permanent one.
if [ -e $DEV_HELPER_HOME_DIR/.dev-helper/history ]; then
    if [ -e $DEV_HELPER_HOME_DIR/.bash_history ]; then
        rm -fv $DEV_HELPER_HOME_DIR/.bash_history;
    fi;
else
    if [ -e $DEV_HELPER_HOME_DIR/.bash_history ]; then
        mv -v $DEV_HELPER_HOME_DIR/.bash_history $DEV_HELPER_HOME_DIR/.dev-helper/history;
    else
        touch $DEV_HELPER_HOME_DIR/.dev-helper/history;
    fi;
fi;
sudo chown -v $DEV_HELPER_PUBLIC_OWNER $DEV_HELPER_HOME_DIR/.dev-helper/history;
ln -s $DEV_HELPER_HOME_DIR/.dev-helper/history $DEV_HELPER_HOME_DIR/.bash_history;
#
# Connection TMux configuration files and directories to more permanent
# locations.
if [ -e $DEV_HELPER_HOME_DIR/.dev-helper/tmux.conf ]; then
    if [ -e $DEV_HELPER_HOME_DIR/.tmux.conf ]; then
        rm -fv $DEV_HELPER_HOME_DIR/.tmux.conf;
    fi;
else
    if [ -e $DEV_HELPER_HOME_DIR/.tmux.conf ]; then
        mv -v $DEV_HELPER_HOME_DIR/.tmux.conf $DEV_HELPER_HOME_DIR/.dev-helper/tmux.conf;
    else
        cat > $DEV_HELPER_HOME_DIR/.dev-helper/tmux.conf << __ENDL__
# List of plugins.
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'tmux-plugins/tmux-sensible'

# Other examples:
# set -g @plugin 'github_username/plugin_name'
# set -g @plugin 'github_username/plugin_name#branch'
# set -g @plugin 'git@github.com:user/plugin'
# set -g @plugin 'git@bitbucket.com:user/plugin'

# Initialize TMUX plugin manager (keep this line at the very bottom of tmux.conf)
run '/usr/share/tmux-plugin-manager/tpm'
__ENDL__
    fi;
fi;
sudo chown -v $DEV_HELPER_PUBLIC_OWNER $DEV_HELPER_HOME_DIR/.dev-helper/tmux.conf;
ln -s $DEV_HELPER_HOME_DIR/.dev-helper/tmux.conf $DEV_HELPER_HOME_DIR/.tmux.conf;

if [ -e $DEV_HELPER_HOME_DIR/.dev-helper/tmux ]; then
    if [ -e $DEV_HELPER_HOME_DIR/.tmux ]; then
        rm -frv $DEV_HELPER_HOME_DIR/.tmux;
    fi;
else
    if [ -e $DEV_HELPER_HOME_DIR/.tmux ]; then
        mv -v $DEV_HELPER_HOME_DIR/.tmux $DEV_HELPER_HOME_DIR/.dev-helper/tmux;
    else
        mkdir $DEV_HELPER_HOME_DIR/.dev-helper/tmux;
    fi;
fi;
sudo chown -v $DEV_HELPER_PUBLIC_OWNER $DEV_HELPER_HOME_DIR/.dev-helper/tmux;
ln -s $DEV_HELPER_HOME_DIR/.dev-helper/tmux $DEV_HELPER_HOME_DIR/.tmux;

if [ -e $DEV_HELPER_HOME_DIR/.dev-helper/tmuxp ]; then
    if [ -e $DEV_HELPER_HOME_DIR/.tmuxp ]; then
        rm -frv $DEV_HELPER_HOME_DIR/.tmuxp;
    fi;
else
    if [ -e $DEV_HELPER_HOME_DIR/.tmuxp ]; then
        mv -v $DEV_HELPER_HOME_DIR/.tmuxp $DEV_HELPER_HOME_DIR/.dev-helper/tmuxp;
    else
        mkdir $DEV_HELPER_HOME_DIR/.dev-helper/tmuxp;
    fi;
fi;
sudo chown -v $DEV_HELPER_PUBLIC_OWNER $DEV_HELPER_HOME_DIR/.dev-helper/tmuxp;
ln -s $DEV_HELPER_HOME_DIR/.dev-helper/tmuxp $DEV_HELPER_HOME_DIR/.tmuxp;
#
# -< Security >----------------------------------------------------------------- #
#
# Enforcing password.
password="$(echo $RANDOM | md5sum | head -c 20)";
if [ "$VSCODER_SECURITY" == 'insecure' ]; then
    password="dev";
elif [ "$VSCODER_SECURITY" == 'password' ]; then
    password="$VSCODER_PASSWORD";
fi
echo "dev:$password" | sudo chpasswd;
#
# -< Rebuilding >--------------------------------------------------------------- #
#
/bin/bash /usr/share/dev-helper/dev-helper.recall.sh &
#
# Executing docker command.
exec $@;
