#!/bin/bash
. /usr/share/dev-helper/dev-helper.constants.sh;
#
COMMAND="$1";
shift;
PACKAGES=$*;
#
MEMORY=$DEV_HELPER_MEMORY_DIR/apt-packs;
#
function forgetPackages {
    for pack in $PACKAGES; do
        if [ -n "$(cat $MEMORY|grep "$pack")" ]; then
            cat $MEMORY | egrep -v "^$pack\$" | sudo tee $MEMORY >/dev/null;
        fi;
    done;
}
function rememberPackages {
    for pack in $PACKAGES; do
        if [ -z "$(cat $MEMORY|grep "$pack")" ]; then
            echo "$pack" | sudo tee -a $MEMORY >/dev/null;
        fi;
    done;
}
#
sudo touch $MEMORY;
if [ "$COMMAND" == "install" ]; then
    if [ -n "$PACKAGES" ]; then
        DEBIAN_FRONTEND=noninteractive TZ=Etc/UTC sudo apt-get install -y $PACKAGES;
        if [ "$?" -eq 0 ]; then
            rememberPackages;
        fi;
    fi;
elif [ "$COMMAND" == "remove" ]; then
    if [ -n "$PACKAGES" ]; then
        sudo apt-get remove -y $PACKAGES;
        if [ "$?" -eq 0 ]; then
            forgetPackages;
        fi;
    fi;
elif [ "$COMMAND" == "recall" ]; then
    DEBIAN_FRONTEND=noninteractive TZ=Etc/UTC sudo apt-get install -y $(cat $MEMORY);
fi;
