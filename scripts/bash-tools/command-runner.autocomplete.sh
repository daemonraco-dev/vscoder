#!/bin/bash
#
# Debian apt-get(8) completion                             -*- shell-script -*-
#
. /usr/share/dev-helper/dev-helper.constants.sh;
#
_dhrun() {
    #
    # Loading auto-completion values.
    local cur prev words cword package;
    _init_completion -n ':=' || return

    local CONFIG_PATH="$DEV_HELPER_HOME_DIR/.dev-helper/command-runner.conf";

    if [ -e "$CONFIG_PATH" ]; then
        local aliasses="$(cat "$CONFIG_PATH" | egrep -v '^(#|HELP)' | cut -d: -f-1 | sort -u)";
        if [ $cword -lt 2 ]; then
            COMPREPLY=($(compgen -W "$aliasses" -- $cur));
        else
            _filedir;
        fi;
    else
        COMPREPLY=();
    fi;
} &&
complete -F _dhrun dhrun

# ex: filetype=sh
