#!/bin/bash
#
alias egrep='egrep --color=auto';
alias fgrep='clear;find . -type f|egrep -v /.git/|xargs egrep -n';
alias grep='grep --color=auto';
alias psg='ps ax|egrep';
alias qiv='qiv -ftN';
alias wwget='wget -nv -c --user-agent="Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.162 Safari/537.36" --no-check-certificate';
