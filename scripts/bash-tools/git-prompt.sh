#!/bin/bash
#
# Redifing the prompt message to contain GIT information.
function GitBranch(){
    git branch 2>/dev/null | egrep '^\*' | awk '($0 != ""){print " (" $2 ")"}'
}
export PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\[\033[01;32m\]$(GitBranch)\[\033[00m\]\$ ';
