#!/bin/bash
. /usr/share/dev-helper/dev-helper.constants.sh;
#
mkdir -p $DEV_HELPER_HOME_DIR/.dev-helper/bin;
if [ ! -e "$DEV_HELPER_HOME_DIR/.dev-helper/bin/ast" ]; then
    ln -s /usr/share/dev-helper/bash-tools/ast $DEV_HELPER_HOME_DIR/.dev-helper/bin/ast;
fi;
sudo chmod a+x /usr/share/dev-helper/bash-tools/ast;
