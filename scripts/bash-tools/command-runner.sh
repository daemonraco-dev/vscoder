#!/bin/bash
. /usr/share/dev-helper/dev-helper.constants.sh;
#
mkdir -p $DEV_HELPER_HOME_DIR/.dev-helper/bin;
if [ ! -e "$DEV_HELPER_HOME_DIR/.dev-helper/bin/dhrun" ]; then
    ln -s /usr/share/dev-helper/bash-tools/dhrun $DEV_HELPER_HOME_DIR/.dev-helper/bin/dhrun;
fi;
sudo chmod a+x /usr/share/dev-helper/bash-tools/dhrun;
#
# Loading autocomplete.
. /usr/share/dev-helper/bash-tools/command-runner.autocomplete.sh;
