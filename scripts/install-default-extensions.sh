#!/bin/bash
. /usr/share/dev-helper/dev-helper.constants.sh;
#
if [ -n "$(which code)" ]; then
    code --install-extension EditorConfig.EditorConfig --force;
    code --install-extension richie5um2.vscode-sort-json --force;
    code --install-extension sgryjp.vscode-stable-sort --force;
    code --install-extension streetsidesoftware.code-spell-checker --force;
    code --install-extension esbenp.prettier-vscode --force;
    code --install-extension tamasfe.even-better-toml --force;
fi;
