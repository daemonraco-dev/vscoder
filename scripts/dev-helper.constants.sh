#!/bin/bash
#
. /usr/share/dev-helper/dev-helper.core-constants.sh;
#
# Functions.
function DEV_HELPER_PROMPT_MESSAGE {
    if [ -e /tmp/dev-helper.rebuild-memory.log ]; then
        echo -e "\e[33mrecalling... \e[0m";
    fi;
}
export -f DEV_HELPER_PROMPT_MESSAGE;
#
# Security.
if [ -z "$(echo "$VSCODER_SECURITY"|egrep '(insecure|password|secure)')" ]; then
    export VSCODER_SECURITY="secure";
fi;
if [ "$VSCODER_SECURITY" == 'password' ] && [ -z "$VSCODER_PASSWORD" ]; then
    export VSCODER_SECURITY="secure";
fi;
#
pieces='';
for a in $DEV_HELPER_INSTALLABLES; do
    sep='';
    if [ -n "$pieces" ]; then
        sep="|";
    fi;
    pieces="$pieces$sep$a";
done;
pattern="^($pieces)$";
export DEV_HELPER_INSTALLABLES_PATTERN="$pattern";
#
pieces='';
for a in $DEV_HELPER_FEATURES; do
    sep='';
    if [ -n "$pieces" ]; then
        sep="|";
    fi;
    pieces="$pieces$sep$a";
done;
pattern="^($pieces)$";
export DEV_HELPER_FEATURES_PATTERN="$pattern";
#
if [ -z "$(echo "$PS1"|grep 'DEV_HELPER_PROMPT_MESSAGE')" ]; then
    export PS1="$PS1\$(DEV_HELPER_PROMPT_MESSAGE)";
fi;
