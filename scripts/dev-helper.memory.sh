#!/bin/bash
. /usr/share/dev-helper/dev-helper.constants.sh;
#
# Enforcing basic structure.
if [ ! -e "$DEV_HELPER_MEMORY_DIR/rebuild.sh" ]; then
    echo '#!/bin/bash' | sudo tee -a "$DEV_HELPER_MEMORY_DIR/rebuild.sh" >/dev/null;
    echo '. /usr/share/dev-helper/dev-helper.constants.sh;' | sudo tee -a "$DEV_HELPER_MEMORY_DIR/rebuild.sh" >/dev/null;
    echo '#' | sudo tee -a "$DEV_HELPER_MEMORY_DIR/rebuild.sh" >/dev/null;
fi;
#
# Memorizing bundle installations.
if [ -n "$1" ] && [ -n "$2" ] && [ "$1" == "install" ]; then
    if [ -n "$(echo "$2" | egrep "$DEV_HELPER_INSTALLABLES_PATTERN")" ]; then
        if [ -z "$(grep "dev-helper install-$2" "$DEV_HELPER_MEMORY_DIR/rebuild.sh")" ]; then
            echo "dev-helper install-$2;" | sudo tee -a "$DEV_HELPER_MEMORY_DIR/rebuild.sh" >/dev/null;
        fi;
    else
        echo -e "\n\e[33mUnable to remember '$2'.\e[0m\n" >&2;
    fi;
fi;
#
# Forgetting bumdle installations.
if [ -n "$1" ] && [ -n "$2" ] && [ "$1" == "uninstall" ]; then
    if [ -n "$(echo "$2" | egrep "$DEV_HELPER_INSTALLABLES_PATTERN")" ]; then
        if [ -n "$(grep "dev-helper install-$2" "$DEV_HELPER_MEMORY_DIR/rebuild.sh")" ]; then
            grep -v "dev-helper install-$2;" "$DEV_HELPER_MEMORY_DIR/rebuild.sh" | sudo tee "$DEV_HELPER_MEMORY_DIR/rebuild.sh" >/dev/null;
        fi;
    else
        echo -e "\n\e[33mUnable to forget '$2'.\e[0m\n" >&2;
    fi;
fi;
#
# Memorizing enabled features.
if [ -n "$1" ] && [ -n "$2" ] && [ "$1" == "enable" ]; then
    if [ -n "$(echo "$2" | egrep "$DEV_HELPER_FEATURES_PATTERN")" ]; then
        if [ -z "$(grep "dev-helper enable-$2" "$DEV_HELPER_MEMORY_DIR/rebuild.sh")" ]; then
            echo "dev-helper enable-$2;" | sudo tee -a "$DEV_HELPER_MEMORY_DIR/rebuild.sh" >/dev/null;
        fi;
    else
        echo -e "\n\e[33mUnable to remember '$2'.\e[0m\n" >&2;
    fi;
fi;
#
# Forgetting enabled features.
if [ -n "$1" ] && [ -n "$2" ] && [ "$1" == "disable" ]; then
    if [ -n "$(echo "$2" | egrep "$DEV_HELPER_FEATURES_PATTERN")" ]; then
        if [ -n "$(grep "dev-helper enable-$2" "$DEV_HELPER_MEMORY_DIR/rebuild.sh")" ]; then
            grep -v "dev-helper enable-$2;" "$DEV_HELPER_MEMORY_DIR/rebuild.sh" | sudo tee "$DEV_HELPER_MEMORY_DIR/rebuild.sh" >/dev/null;
        fi;
    else
        echo -e "\n\e[33mUnable to forget '$2'.\e[0m\n" >&2;
    fi;
fi;
