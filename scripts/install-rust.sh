#!/bin/bash
. /usr/share/dev-helper/dev-helper.constants.sh;
#
if [ -e $DEV_HELPER_HOME_DIR/.cargo ]; then
    sudo chown -R $DEV_HELPER_PUBLIC_OWNER $DEV_HELPER_HOME_DIR/.cargo;
fi;
#
/bin/bash /usr/share/dev-helper/install-build-essential.sh;
#
if [ ! -e $DEV_HELPER_HOME_DIR/.cargo/bin/cargo ]; then
    curl https://sh.rustup.rs -sSf | sh -s -- -y --no-modify-path;
fi;
#
if [ -z "$(cat $DEV_HELPER_HOME_DIR/.dev-helper/bashrc | grep '.cargo/env')" ]; then
    echo '. "$HOME/.cargo/env"' >> $DEV_HELPER_HOME_DIR/.dev-helper/bashrc;
fi;
#
/bin/bash /usr/share/dev-helper/install-default-extensions.sh;
/bin/bash /usr/share/dev-helper/install-rust-extensions.sh;
#
# Remembering
/bin/bash /usr/share/dev-helper/dev-helper.memory.sh install rust;
