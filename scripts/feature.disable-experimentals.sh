#!/bin/bash
#
. /usr/share/dev-helper/dev-helper.constants.sh;
#
if [ "$DEV_HELPER_EXPERIMENTALS_ENABLED" == "ENABLED" ]; then
    #
    # Forgeting installed experimental bundles.
    echo -e "\e[32mForgeting installed experimental bundles...\e[0m";
    if [ ! "$DEV_HELPER_EXPERIMENTAL_INSTALLABLES" == "" ]; then
        for bundle in $DEV_HELPER_EXPERIMENTAL_INSTALLABLES; do
            /bin/bash /usr/share/dev-helper/dev-helper.memory.sh uninstall $bundle;
        done;
    fi;
    #
    # Forgeting enabled experimental features.
    echo -e "\e[32mForgeting enabled experimental features...\e[0m";
    if [ ! "$DEV_HELPER_EXPERIMENTAL_FEATURES" == "" ]; then
        for feature in $DEV_HELPER_EXPERIMENTAL_FEATURES; do
            sudo /bin/bash /usr/share/dev-helper/dev-helper disable-$feature;
        done;
    fi;
    #
    # Removing experimentals from the system.
    echo -e "\e[32mRemoving experimentals from the system...\e[0m";
    sudo rm -fr "$DEV_HELPER_EXPERIMENTALS_DIR";
    #
    # Forgeting.
    echo -e "\e[32mForgeting that experimentals were enabled...\e[0m";
    /bin/bash /usr/share/dev-helper/dev-helper.memory.sh disable experimentals;
else
    echo -e "\n\e[33mDevHelper experimentals are not enabled.\e[0m" >&2;
fi;
