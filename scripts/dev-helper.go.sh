#!/bin/bash
. /usr/share/dev-helper/dev-helper.constants.sh;
#
COMMAND="$1";
shift;
PACKAGES=$*;
#
MEMORY=$DEV_HELPER_MEMORY_DIR/go-packs;
#
function rememberPackages {
    for pack in $PACKAGES; do
        noVersionPack="${pack%@*}"

        cat <<__ENDL__ | grep -v '^$' | sudo tee $MEMORY >/dev/null;
$(cat $MEMORY | grep -v "$noVersionPack")
$pack
__ENDL__
    done;
}
#
sudo touch $MEMORY;
if [ "$COMMAND" == "install" ]; then
    if [ -n "$PACKAGES" ]; then
        if [ -e "/usr/local/go/bin/go" ]; then
            sudo -u dev /usr/local/go/bin/go install $PACKAGES;
            if [ "$?" -eq 0 ]; then
                rememberPackages;
            fi;
        else
            echo -e "\e[31mGo is not intalled yet.\e[0m" >&2;
            echo -e "\e[31mTry running 'dev-helper install-go' first.\e[0m" >&2;
        fi;
    fi;
elif [ "$COMMAND" == "recall" ]; then
    memPacks=$(cat $MEMORY);
    if [ -n "$memPacks" ]; then
        if [ -e "/usr/local/go/bin/go" ]; then
            cat $MEMORY | xargs -I {} sudo -u dev /usr/local/go/bin/go install {};
        else
            echo -e "\e[31mGo is not intalled yet.\e[0m" >&2;
            echo -e "\e[31mTry running 'dev-helper install-go' first.\e[0m" >&2;
        fi;
    fi;
fi;
