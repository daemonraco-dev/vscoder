#!/bin/bash
#
export DEV_HELPER_EXPERIMENTALS_DIR="/usr/share/dev-helper-experimentals";
export DEV_HELPER_FEATURES="experimentals web-coder";
export DEV_HELPER_HOME_DIR="/home/dev";
export DEV_HELPER_INSTALLABLES_ALIAS="nvim:neovim nest:nestjs node-lts:node-20 node:node-21 php:php-apache";
export DEV_HELPER_INSTALLABLES="angular c docker go neovim nest nestjs node node-16 node-18 node-19 node-20 node-21 node-lts nvim php php-apache php-nginx react rust vue";
export DEV_HELPER_PUBLIC_OWNER='dev:root';
export DEV_HELPER_ROOT_DIR="/dev-helper";
#
export DEV_HELPER_MEMORY_DIR="$DEV_HELPER_ROOT_DIR/memory";
export DEV_HELPER_VSCODE_DIR="$DEV_HELPER_HOME_DIR/.vscode-server";
#
export DEV_HELPER_EXPERIMENTALS_REPO="https://gitlab.com/daemonraco-dev/vscoder-experimentals.git";
export DEV_HELPER_EXPERIMENTALS_BRANCH="master";
#
# Experimentals configuration.
if [ -e $DEV_HELPER_EXPERIMENTALS_DIR/constants.sh ]; then
    . $DEV_HELPER_EXPERIMENTALS_DIR/constants.sh;
fi;
#
# Custom configuration.
if [ -e $DEV_HELPER_ROOT_DIR/constants.sh ]; then
    . $DEV_HELPER_ROOT_DIR/constants.sh;
fi;
#
export DEV_HELPER_EXPERIMENTALS_ENABLED="";
if [ -e "$DEV_HELPER_EXPERIMENTALS_DIR" ]; then
    export DEV_HELPER_EXPERIMENTALS_ENABLED="ENABLED";
fi;
