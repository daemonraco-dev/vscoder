#!/bin/bash
. /usr/share/dev-helper/dev-helper.constants.sh;
#
if [ -z "$(grep 'npm completion' "$DEV_HELPER_HOME_DIR/.bashrc")" ]; then
    echo ". <(npm completion);" >> "$DEV_HELPER_HOME_DIR/.bashrc";
fi;
