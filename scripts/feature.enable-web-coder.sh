#!/bin/bash
#
# Web Coder is based on https://github.com/coder/code-server
#
. /usr/share/dev-helper/feature.web-coder.sh;
#
curDir="$PWD";
#
if [ ! -e "$serviceDestination" ]; then
    cd /tmp;
    #
    # Version.
    echo -e "\e[32mGuessing web-coder server version...\e[0m";
    version="$(curl https://api.github.com/repos/coder/code-server/releases 2>/dev/null|grep browser_download_url)";
    version="$(echo "$version"|awk -F\" '{print $4}'|grep linux-arm64|head -1)";
    version="$(echo "$version"|awk -F/ '{print $8}'|cut -c2-)";
    #
    # Downloading server.
    echo -e "\e[32mDownloading web-coder server...\e[0m";
    if [ -e "code-server_${version}_amd64.deb" ]; then
        sudo rm -f "code-server_${version}_amd64.deb";
    fi;
    curl -fOL "https://github.com/cdr/code-server/releases/download/v${version}/code-server_${version}_amd64.deb";
    #
    # Installing binary.
    echo -e "\n\e[32mInstalling web-coder binary...\e[0m";
    sudo dpkg -i "code-server_${version}_amd64.deb";
    #
    # Installing service.
    echo -e "\n\e[32mInstalling web-coder service...\e[0m";
    sudo cp "$serviceSource" "$serviceDestination";
    sudo update-rc.d "$serviceName" defaults;
    #
    # Remembering.
    echo -e "\e[32mRemembering that web-coder is enabled...\e[0m";
    /bin/bash /usr/share/dev-helper/dev-helper.memory.sh enable $serviceName;
else
    echo -e "\n\e[33mWeb-coder is already enabled.\e[0m" >&2;
fi;
#
# Starting service.
echo -e "\e[32mStarting web-coder service...\e[0m";
sudo service "$serviceName" start;

cd "$curDir";
