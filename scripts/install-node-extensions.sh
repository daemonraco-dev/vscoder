#!/bin/bash
. /usr/share/dev-helper/dev-helper.constants.sh;
#
if [ -n "$(which code)" ]; then
    code --install-extension dbaeumer.vscode-eslint --force;
    code --install-extension krizzdewizz.tag-rename --force;
    code --install-extension mrmlnc.vscode-scss --force;
    code --install-extension ritwickdey.live-sass --force;
    code --install-extension ritwickdey.liveserver --force;
    code --install-extension sibiraj-s.vscode-scss-formatter --force;
    code --install-extension jeremyrajan.webpack --force;
else
    /bin/bash /usr/share/dev-helper/dev-helper.vscode-check.sh;
fi;
