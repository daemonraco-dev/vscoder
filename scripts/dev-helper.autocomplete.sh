#!/bin/bash
#
# Debian apt-get(8) completion                             -*- shell-script -*-
#
. /usr/share/dev-helper/dev-helper.constants.sh;
#
_dev_helper() {
    #
    # Loading auto-completion values.
    local cur prev words cword package
    _init_completion -n ':=' || return

    local SUPER_COMMAND COMMAND;
    #
    # Known commands.
    local aptOptions="apt-install apt-remove";
    local coreOptions="recall recall-log rl";
    local goOptions="go-install";
    local npmOptions="npm-install npm-remove";
    local installOptions='';
    local featureOptions='';
    for a in $DEV_HELPER_INSTALLABLES; do
        installOptions="$installOptions install-$a";
    done;
    for a in $DEV_HELPER_FEATURES; do
        featureOptions="$featureOptions enable-$a disable-$a";
    done;
    #
    # Calculating command.
    local i
    for (( i=0; i < ${#words[@]}-1; i++ )); do
        if [ -n "$(printf '%s\n' ${installOptions[@]} | grep -P "^${words[i]}$")" ]; then
            SUPER_COMMAND='install';
            COMMAND=${words[i]};
        elif [ -n "$(printf '%s\n' ${featureOptions[@]} | grep -P "^${words[i]}$")" ]; then
            SUPER_COMMAND='feature';
            COMMAND=${words[i]};
        elif [ -n "$(printf '%s\n' ${aptOptions[@]} | grep -P "^${words[i]}$")" ]; then
            SUPER_COMMAND='apt';
            COMMAND=${words[i]};
        elif [ -n "$(printf '%s\n' ${npmOptions[@]} | grep -P "^${words[i]}$")" ]; then
            SUPER_COMMAND='npm';
            COMMAND=${words[i]};
        elif [ -n "$(printf '%s\n' ${coreOptions[@]} | grep -P "^${words[i]}$")" ]; then
            SUPER_COMMAND='core';
            COMMAND=${words[i]};
        fi;
    done;
    #
    # Generating suggestions.
    local useGeneric='';
    case $SUPER_COMMAND in
        apt)
            if [ "$COMMAND" == "apt-install" ]; then
                COMPREPLY=( $(apt-cache --no-generate pkgnames "$cur" \
                    2>/dev/null) $(compgen -W "$(apt-cache dumpavail |
                    awk '$1 == "Source:" { print $2 }' | sort -u)" -- "$cur") );
            elif [ "$COMMAND" == "apt-remove" ]; then
                COMPREPLY=( $(_xfunc dpkg _comp_dpkg_installed_packages $cur) );
            else
                useGeneric='useGeneric';
            fi;
            ;;

        core|feature|install)
            COMPREPLY='';
            ;;

        npm)
            COMPREPLY='';
            ;;

        *)
            useGeneric='useGeneric';
            ;;
    esac;

    if [ -n "$useGeneric" ]; then
        COMPREPLY=( $(compgen -W "$aptOptions $coreOptions $featureOptions $installOptions $npmOptions $goOptions" -- $cur) );
    fi;
} &&
complete -F _dev_helper dev-helper

# ex: filetype=sh
