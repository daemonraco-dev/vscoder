#!/bin/bash
#
# Web Coder is based on https://github.com/coder/code-server
#
. /usr/share/dev-helper/dev-helper.constants.sh;
#
serviceName="web-coder";
serviceSource="/usr/share/dev-helper/web-coder/${serviceName}.service";
serviceDestination="/etc/init.d/${serviceName}";
