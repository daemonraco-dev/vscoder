#!/bin/bash
#
. /usr/share/dev-helper/dev-helper.constants.sh;
#
curDir="$PWD";
loginSuggestions="";
#
# Enabling feature.
if [ "$DEV_HELPER_EXPERIMENTALS_ENABLED" == "" ]; then
    #
    # Cloning experimentals repository.
    echo -e "\e[32mCloning experimentals repository...\e[0m";
    sudo git clone "$DEV_HELPER_EXPERIMENTALS_REPO" "$DEV_HELPER_EXPERIMENTALS_DIR";
    . /usr/share/dev-helper/dev-helper.constants.sh;
    #
    # Remembering.
    echo -e "\e[32mRemembering that experimentals are enabled...\e[0m";
    /bin/bash /usr/share/dev-helper/dev-helper.memory.sh enable experimentals;
    #
    loginSuggestions="true";
else
    echo -e "\n\e[33mDevHelper experimentals are already enabled.\e[0m" >&2;
fi;
#
# Updating repository.
echo -e "\e[32mUpdating experimentals...\e[0m";
cd "$DEV_HELPER_EXPERIMENTALS_DIR";
sudo git checkout "$DEV_HELPER_EXPERIMENTALS_BRANCH";
sudo git pull;
#
cd "$curDir";
#
if [ -n "$loginSuggestions" ]; then
    echo -e "\n\e[33mSome system configurations have changed.\e[0m" >&2;
    echo -e "\e[33mIt is recommended that you close your current session and log back in.\e[0m" >&2;
fi;
