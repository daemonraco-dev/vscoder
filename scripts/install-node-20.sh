#!/bin/bash
. /usr/share/dev-helper/dev-helper.constants.sh;
#
/bin/bash /usr/share/dev-helper/install-build-essential.sh;
#
if [ ! -e "/etc/apt/sources.list.d/nodesource.list" ]; then
    curl -fsSL https://deb.nodesource.com/setup_20.x | sudo -E bash -;
    sudo apt-get install -y nodejs;
fi;
#
/bin/bash /usr/share/dev-helper/install-default-extensions.sh;
/bin/bash /usr/share/dev-helper/install-node-extensions.sh;
/bin/bash /usr/share/dev-helper/install-node-autocomplete.sh;
#
# Remembering
/bin/bash /usr/share/dev-helper/dev-helper.memory.sh install node-20;
