#!/bin/bash
. /usr/share/dev-helper/dev-helper.constants.sh;
#
if [ -n "$(which code)" ]; then
    code --install-extension golang.Go --force;

    go get -v golang.org/x/tools/gopls;
else
    /bin/bash /usr/share/dev-helper/dev-helper.vscode-check.sh;
fi;
