#!/bin/bash
. /usr/share/dev-helper/dev-helper.constants.sh;
#
/bin/bash /usr/share/dev-helper/install-build-essential.sh;
#
/bin/bash /usr/share/dev-helper/install-default-extensions.sh;
/bin/bash /usr/share/dev-helper/install-c-extensions.sh;
#
# Remembering.
/bin/bash /usr/share/dev-helper/dev-helper.memory.sh install c;
