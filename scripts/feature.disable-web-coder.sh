#!/bin/bash
#
# Web Coder is based on https://github.com/coder/code-server
#
. /usr/share/dev-helper/feature.web-coder.sh;
#
if [ -e "$serviceDestination" ]; then
    #
    # Stopping service.
    echo -e "\e[32mStopping web-coder service...\e[0m";
    sudo service "$serviceName" stop;
    #
    # Uninstalling binary.
    echo -e "\n\e[32mUninstalling web-coder binary...\e[0m";
    sudo apt-get remove -y code-server;
    #
    # Uninstalling service.
    echo -e "\n\e[32mUninstalling web-coder service...\e[0m";
    sudo update-rc.d "$serviceName" remove;
    sudo rm -f "$serviceDestination";
    #
    # Forgeting.
    echo -e "\e[32mForgeting that web-coder is enabled...\e[0m";
    /bin/bash /usr/share/dev-helper/dev-helper.memory.sh disable $serviceName;

else
    echo -e "\n\e[33mWeb-coder is not enabled.\e[0m" >&2;
fi;
