#!/bin/bash
. /usr/share/dev-helper/dev-helper.constants.sh;
#
if [ -n "$(which code)" ]; then
    code --install-extension ahmadalli.vscode-nginx-conf --force;
    code --install-extension bmewburn.vscode-intelephense-client --force;
    code --install-extension mrmlnc.vscode-apache --force;
else
    /bin/bash /usr/share/dev-helper/dev-helper.vscode-check.sh;
fi;
