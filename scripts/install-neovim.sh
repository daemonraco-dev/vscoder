#!/bin/bash
. /usr/share/dev-helper/dev-helper.constants.sh;
#
CUR_DIR="$PWD";
NVIM_DIR="/usr/share/dev-helper/neovim";
#
if [ ! -e "$NVIM_DIR" ]; then
    #
    # Changing workdirectory.
    cd "/tmp";
    #
    # Guessing the package url.
    assetsUrl=$(curl "https://api.github.com/repos/neovim/neovim/releases/latest" 2>/dev/null | grep assets_url | cut -d'"' -f4);
    imageUrl=$(curl "$assetsUrl" 2>/dev/null | grep '/nvim.appimage"' | cut -d'"' -f4);
    #
    # Downloading and expanding the image.
    echo -e "\e[32mDownloading NeoVim from '$imageUrl'...\e[0m";
    wget -nv "$imageUrl" >/dev/null;
    echo -e "\e[32mExpanding NeoVim...\e[0m";
    chmod +x nvim.appimage;
    ./nvim.appimage --appimage-extract >/dev/null;
    #
    # Installing.
    echo -e "\e[32mInstalling NeoVim...\e[0m";
    sudo mv squashfs-root "$NVIM_DIR";
    sudo mv "$NVIM_DIR/AppRun" "$NVIM_DIR/nvim";
fi;
#
# Enforcing PATH configuration.
if [ -z "$(grep "$NVIM_DIR" "$DEV_HELPER_HOME_DIR/.bashrc")" ]; then
    echo 'export PATH="'"$NVIM_DIR"':$PATH";' >> "$DEV_HELPER_HOME_DIR/.bashrc";
    export PATH="$NVIM_DIR:$PATH";
fi;
#
# Remembering
/bin/bash /usr/share/dev-helper/dev-helper.memory.sh install neovim;
