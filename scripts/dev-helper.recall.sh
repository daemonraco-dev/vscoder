#!/bin/bash
. /usr/share/dev-helper/dev-helper.constants.sh;
#
DEV_HELPER_RECALL_LOG=/tmp/dev-helper.rebuild-memory.log;
#
if [ ! -e $DEV_HELPER_RECALL_LOG ]; then
    sudo touch $DEV_HELPER_RECALL_LOG;
    #
    # Recalling dev-helper installation.
    if [ -e $DEV_HELPER_MEMORY_DIR/rebuild.sh ]; then
        /bin/bash $DEV_HELPER_MEMORY_DIR/rebuild.sh | sudo tee -a $DEV_HELPER_RECALL_LOG;
    fi;
    #
    # Recalling apt installations through dev-helper.
    /bin/bash /usr/share/dev-helper/dev-helper.apt.sh recall | sudo tee -a $DEV_HELPER_RECALL_LOG;
    #
    # Recalling NPM installations through dev-helper.
    /bin/bash /usr/share/dev-helper/dev-helper.npm.sh recall | sudo tee -a $DEV_HELPER_RECALL_LOG;
    #
    # Recalling Golang installations through dev-helper.
    /bin/bash /usr/share/dev-helper/dev-helper.go.sh recall | sudo tee -a $DEV_HELPER_RECALL_LOG;
    #
    # Done.
    echo | sudo tee -a $DEV_HELPER_RECALL_LOG;
    echo "Great! Now I remember." | sudo tee -a $DEV_HELPER_RECALL_LOG;

    sudo cat $DEV_HELPER_RECALL_LOG | sudo tee -a ${DEV_HELPER_RECALL_LOG%.log}.done >/dev/null;
    sudo rm -fv $DEV_HELPER_RECALL_LOG;
else
    echo -e "\e[33mAlready recalling.\e[0m" >&2;
fi;
