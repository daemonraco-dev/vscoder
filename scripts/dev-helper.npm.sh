#!/bin/bash
. /usr/share/dev-helper/dev-helper.constants.sh;
#
COMMAND="$1";
shift;
PACKAGES=$*;
#
MEMORY=$DEV_HELPER_MEMORY_DIR/npm-packs;
#
function forgetPackages {
    for pack in $PACKAGES; do
        if [ -n "$(cat $MEMORY|grep "$pack")" ]; then
            cat $MEMORY | egrep -v "^$pack\$" | sudo tee $MEMORY >/dev/null;
        fi;
    done;
}
function rememberPackages {
    for pack in $PACKAGES; do
        if [ -z "$(cat $MEMORY|grep "$pack")" ]; then
            echo "$pack" | sudo tee -a $MEMORY >/dev/null;
        fi;
    done;
}
#
sudo touch $MEMORY;
if [ "$COMMAND" == "install" ]; then
    if [ -n "$PACKAGES" ]; then
        if [ -n "$(which node)" ]; then
            sudo npm install --global $PACKAGES;
            if [ "$?" -eq 0 ]; then
                rememberPackages;
            fi;
        else
            echo -e "\e[31mNodeJS is not intalled yet.\e[0m" >&2;
            echo -e "\e[31mTry running 'dev-helper install-node' first.\e[0m" >&2;
        fi;
    fi;
elif [ "$COMMAND" == "remove" ]; then
    if [ -n "$PACKAGES" ]; then
        if [ -n "$(which node)" ]; then
            sudo npm remove --global $PACKAGES;
            if [ "$?" -eq 0 ]; then
                forgetPackages;
            fi;
        else
            echo -e "\e[31mNodeJS is not intalled yet.\e[0m" >&2;
            echo -e "\e[31mTry running 'dev-helper install-node' first.\e[0m" >&2;
        fi;
    fi;
elif [ "$COMMAND" == "recall" ]; then
    memPacks=$(cat $MEMORY);
    if [ -n "$memPacks" ]; then
        if [ -n "$(which node)" ]; then
            sudo npm install --global $(cat $MEMORY);
        else
            echo -e "\e[31mNodeJS is not intalled yet.\e[0m" >&2;
            echo -e "\e[31mTry running 'dev-helper install-node' first.\e[0m" >&2;
        fi;
    fi;
fi;
