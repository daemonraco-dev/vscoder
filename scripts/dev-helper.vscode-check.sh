#!/bin/bash
. /usr/share/dev-helper/dev-helper.constants.sh;
#
if [ -z "$(which code)" ]; then
    echo -e "\n\e[33mVSCode is not installed yet or you're not running this from inside it.\e[0m" >&2;
    echo -e "\e[33mAfter you remotely access with it, try running 'dev-helper recall' in a terminal inside VSCode.\e[0m\n" >&2;
fi;
