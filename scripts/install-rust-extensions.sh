#!/bin/bash
. /usr/share/dev-helper/dev-helper.constants.sh;
#
if [ -n "$(which code)" ]; then
    code --install-extension rust-lang.rust --force;
    code --install-extension serayuzgur.crates --force;
    code --install-extension tamasfe.even-better-toml --force;
else
    /bin/bash /usr/share/dev-helper/dev-helper.vscode-check.sh;
fi;
