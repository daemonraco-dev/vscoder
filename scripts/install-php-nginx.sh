#!/bin/bash
. /usr/share/dev-helper/dev-helper.constants.sh;
#
if [ ! -e "/usr/sbin/apache2" ]; then
    sudo apt-get install nginx php-fpm -y;

    phpfpmService=$(sudo service --status-all 2>/dev/null|grep php|sed 's:.*php:php:g');
    phpfpmSocket=$(ls -1 /var/run/php/php*-fpm.sock);
    sudo service $phpfpmService start;

    sudo mkdir -vp $DEV_HELPER_HOME_DIR/www;
    sudo chown -vR $DEV_HELPER_PUBLIC_OWNER $DEV_HELPER_HOME_DIR/www;
    sudo chmod -v 0755 $DEV_HELPER_HOME_DIR $DEV_HELPER_HOME_DIR/www;

    if [ ! -e $DEV_HELPER_HOME_DIR/www/index.php ]; then
        cat << __ENDL__ | sudo tee $DEV_HELPER_HOME_DIR/www/index.php >/dev/null
<?php phpinfo() ?>
__ENDL__
        sudo chown -v $DEV_HELPER_PUBLIC_OWNER $DEV_HELPER_HOME_DIR/www/index.php;
        sudo chmod -v 0644 $DEV_HELPER_HOME_DIR/www/index.php;
    fi;

    cat << __ENDL__ | sudo tee /etc/nginx/sites-available/dev-helper >/dev/null
server {
	listen 80 default_server;
	listen [::]:80 default_server;

	root /home/dev/www;

	index index.php index.html index.htm index.nginx-debian.html;

	server_name _;

	location / {
		try_files \$uri \$uri/ =404;
	}

	location ~ \.php\$ {
		include snippets/fastcgi-php.conf;
		fastcgi_pass unix:$phpfpmSocket;
	}
}
__ENDL__

    sudo unlink /etc/nginx/sites-enabled/default;
    sudo ln -s /etc/nginx/sites-available/dev-helper /etc/nginx/sites-enabled/dev-helper;

    sudo service nginx restart;
fi;
#
/bin/bash /usr/share/dev-helper/install-default-extensions.sh;
/bin/bash /usr/share/dev-helper/install-php-extensions.sh;
#
# Remembering
/bin/bash /usr/share/dev-helper/dev-helper.memory.sh install php-nginx;
