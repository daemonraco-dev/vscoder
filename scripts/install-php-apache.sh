#!/bin/bash
. /usr/share/dev-helper/dev-helper.constants.sh;
#
if [ ! -e "/usr/sbin/apache2" ]; then
    sudo apt-get install -y apache2 php;

    sudo mkdir -vp $DEV_HELPER_HOME_DIR/www;
    sudo chown -vR $DEV_HELPER_PUBLIC_OWNER $DEV_HELPER_HOME_DIR/www;
    sudo chmod -v 0755 $DEV_HELPER_HOME_DIR $DEV_HELPER_HOME_DIR/www;

    cat << __ENDL__ | sudo tee /etc/apache2/conf-available/dev-helper.conf >/dev/null
<Directory />
	Options Indexes FollowSymLinks Includes
	AllowOverride All
	Require all granted
</Directory>
__ENDL__

    cat << __ENDL__ | sudo tee /etc/apache2/sites-available/000-dev-helper.conf >/dev/null
<VirtualHost *:80>
	ServerAdmin webmaster@localhost
	DocumentRoot ${DEV_HELPER_HOME_DIR}/www

	ErrorLog \${APACHE_LOG_DIR}/error.log
	CustomLog \${APACHE_LOG_DIR}/access.log combined
</VirtualHost>

# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
__ENDL__

    sudo unlink /etc/apache2/sites-enabled/000-default.conf;
    sudo ln -s ../sites-available/000-dev-helper.conf /etc/apache2/sites-enabled/000-dev-helper.conf;
    sudo ln -s ../conf-available/dev-helper.conf /etc/apache2/conf-enabled/dev-helper.conf;

    sudo service apache2 restart;
fi;
#
/bin/bash /usr/share/dev-helper/install-default-extensions.sh;
/bin/bash /usr/share/dev-helper/install-php-extensions.sh;
#
# Remembering
/bin/bash /usr/share/dev-helper/dev-helper.memory.sh install php-apache;
