#!/bin/bash
. /usr/share/dev-helper/dev-helper.constants.sh;
#
if [ -n "$(which code)" ]; then
    code --install-extension octref.vetur --force;
else
    /bin/bash /usr/share/dev-helper/dev-helper.vscode-check.sh;
fi;
