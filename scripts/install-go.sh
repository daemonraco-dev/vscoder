#!/bin/bash
. /usr/share/dev-helper/dev-helper.constants.sh;
#
if [ ! -e "/usr/local/go" ]; then
    version="$(git ls-remote --sort=version:refname --tags https://go.googlesource.com/go|grep 'refs/tags/go')";
    version="$(echo "$version"|egrep -v '(rc|beta|alpha)'|awk '{print $2}'|tail -1|sed 's:refs/tags/go::g')";

    wget -c https://dl.google.com/go/go$version.linux-amd64.tar.gz -O - | sudo tar -xz -C /usr/local;
fi;
if [ -z "$(grep '/usr/local/go/bin' "$DEV_HELPER_HOME_DIR/.bashrc")" ]; then
    echo 'export PATH="$PATH:/usr/local/go/bin";' >> "$DEV_HELPER_HOME_DIR/.bashrc";
    export PATH="$PATH:/usr/local/go/bin";
fi;
if [ -z "$(grep '/home/dev/go/bin' "$DEV_HELPER_HOME_DIR/.bashrc")" ]; then
    echo 'export PATH="$PATH:/home/dev/go/bin";' >> "$DEV_HELPER_HOME_DIR/.bashrc";
    export PATH="$PATH:/home/dev/go/bin";
fi;
sudo chown 0755 /usr/local/go;
#
if [ -e /home/dev/go ]; then
    sudo chown -vR $DEV_HELPER_PUBLIC_OWNER /home/dev/go 2>&1 | grep -v retained;
fi
#
/bin/bash /usr/share/dev-helper/install-default-extensions.sh;
/bin/bash /usr/share/dev-helper/install-go-extensions.sh;
#
# Remembering
/bin/bash /usr/share/dev-helper/dev-helper.memory.sh install go;
