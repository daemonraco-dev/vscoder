#!/bin/bash
. /usr/share/dev-helper/dev-helper.constants.sh;
#
if [ -n "$(which code)" ]; then
    code --install-extension angular.ng-template --force;
else
    /bin/bash /usr/share/dev-helper/dev-helper.vscode-check.sh;
fi;
