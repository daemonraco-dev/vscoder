import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { YmlBuilderComponent } from './yml-builder/yml-builder.component';

const routes: Routes = [
    { path: '', component: YmlBuilderComponent },
    { path: 'about', loadChildren: () => import('./about').then(m => m.AboutModule) },

    { path: '**', redirectTo: '/' },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class PagesRoutingModule { }
