import { CommonModule } from '@angular/common';
import { FileSaverModule } from 'ngx-filesaver';
import { FormsModule } from '@angular/forms';
import { HighlightPlusModule } from 'ngx-highlightjs/plus';
import { NgModule } from '@angular/core';

import { PagesRoutingModule } from './pages-routing.module';

import { DownloaderComponent } from './yml-builder/downloader/downloader.component';
import { FormComponent } from './yml-builder/form/form.component';
import { PreviewComponent } from './yml-builder/preview/preview.component';
import { YmlBuilderComponent } from './yml-builder/yml-builder.component';

@NgModule({
    declarations: [
        DownloaderComponent,
        FormComponent,
        PreviewComponent,
        YmlBuilderComponent,
    ],
    imports: [
        CommonModule,
        FileSaverModule,
        FormsModule,
        HighlightPlusModule,
        PagesRoutingModule,
    ],
    providers: [],
})
export class PagesModule { }
