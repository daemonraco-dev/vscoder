import { Component, OnInit } from '@angular/core';

import { IYMLConfiguration, IYMLConfigurationSecurity } from '@types';
import { YmlConfigurationService } from '@services';

@Component({
    selector: 'app-yml-builder-form',
    templateUrl: './form.component.html',
})
export class FormComponent implements OnInit {
    //
    // Properties.
    public config: IYMLConfiguration = {};
    public readonly IYMLConfigurationSecurity = IYMLConfigurationSecurity;
    //
    // Construction.
    constructor(protected ymlConfigurationSrv: YmlConfigurationService) {
    }
    //
    // Public methods.
    public ngOnInit(): void {
        this.ymlConfigurationSrv.reset();
        this.config = this.ymlConfigurationSrv.get();
    }
    public update(): void {
        if (this.config.directories && !this.config.directories?.sshSeparated) {
            this.config.directories.ssh = `${this.config.directories.cache}/ssh`;
        }

        this.ymlConfigurationSrv.set(this.config);
    }
}
