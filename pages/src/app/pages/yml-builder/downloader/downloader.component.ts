import { Component, OnInit } from '@angular/core';

import { AutoUnsubscribe } from '@decorators';
import { ZipConfigurationService } from '@services';

@Component({
    selector: 'app-yml-builder-downloader',
    templateUrl: './downloader.component.html',
})
@AutoUnsubscribe()
export class DownloaderComponent implements OnInit {
    //
    // Construction.
    constructor(protected zipConfigurationSrv: ZipConfigurationService) {
    }
    //
    // Public methods.
    public download(): void {
        this.zipConfigurationSrv.download();
    }
    public ngOnInit(): void {
    }
}
