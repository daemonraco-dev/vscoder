import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import { AutoUnsubscribe } from '@decorators';
import { IYMLConfiguration } from '@types';
import { YmlConfigurationService } from '@services';

@Component({
    selector: 'app-yml-builder-preview',
    templateUrl: './preview.component.html',
})
@AutoUnsubscribe()
export class PreviewComponent implements OnInit {
    //
    // Properties.
    protected configSubscription: Subscription | null = null;
    protected rebuildShSubscription: Subscription | null = null;
    protected ymlSubscription: Subscription | null = null;
    public config: IYMLConfiguration = {};
    public rebuildSh: string = '';
    public rebuildShPath: string = '';
    public yml: string = '';
    //
    // Construction.
    constructor(protected ymlConfigurationSrv: YmlConfigurationService) {
    }
    //
    // Public methods.
    public ngOnInit(): void {
        this.configSubscription = this.ymlConfigurationSrv.$config
            .subscribe((config: IYMLConfiguration): void => this.checkConfig(config));
        this.rebuildShSubscription = this.ymlConfigurationSrv.$rebuildSh
            .subscribe((rebuildSh: string): void => { this.rebuildSh = rebuildSh; });
        this.ymlSubscription = this.ymlConfigurationSrv.$yml
            .subscribe((yml: string): void => { this.yml = yml; });
    }
    //
    // Protected methods.
    protected checkConfig(config: IYMLConfiguration): void {
        if (`${config.directories?.cache || ''}`.match(/^\.\//)) {
            this.rebuildShPath = `${config.directories?.cache}/rebuild.sh`;
        }
    }
}
