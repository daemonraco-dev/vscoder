import { FileSaverService } from 'ngx-filesaver';
import { Injectable } from '@angular/core';
import * as JSZip from 'jszip';

import { IYMLConfiguration } from '@types';
import { YmlConfigurationService } from './yml-configuration.service';

@Injectable({
    providedIn: 'root',
})
export class ZipConfigurationService {
    //
    // Properties.

    //
    // Construction.
    constructor(
        protected fileSaverSrv: FileSaverService,
        protected ymlConfigurationSrv: YmlConfigurationService) {
    }
    //
    // Public methods.
    public async download(): Promise<void> {
        const config: IYMLConfiguration = this.ymlConfigurationSrv.get();
        const zip: JSZip = new JSZip();
        //
        // Adding the main configuration.
        zip.file('docker-compose.yml', this.ymlConfigurationSrv.getYml());
        //
        // 'rebuild.sh'.
        if (`${config.directories?.cache}`.match(/^\.\/.*/)) {
            const memory = zip.folder(`${config.directories?.cache}/memory`.substring(2));
            memory?.file('rebuild.sh', this.ymlConfigurationSrv.getRebuildSH());
        }
        //
        // Downloading.
        const content: Blob = await zip.generateAsync({ type: 'blob' });
        this.fileSaverSrv.save(content, 'configuration.zip');
    }
    //
    // Protected methods.

}
