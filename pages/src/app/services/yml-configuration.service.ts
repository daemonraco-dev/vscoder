import { BehaviorSubject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';

import { IYMLConfiguration, IYMLConfigurationSecurity } from '../types/yml-configuration';

@Injectable({
    providedIn: 'root',
})
export class YmlConfigurationService {
    //
    // Properties.
    protected config: BehaviorSubject<IYMLConfiguration> = new BehaviorSubject<IYMLConfiguration>({});
    protected rebuildSh: BehaviorSubject<string> = new BehaviorSubject<string>('');
    protected yml: BehaviorSubject<string> = new BehaviorSubject<string>('');
    public $config: Observable<IYMLConfiguration> = this.config.asObservable();
    public $rebuildSh: Observable<string> = this.rebuildSh.asObservable();
    public $yml: Observable<string> = this.yml.asObservable();
    //
    // Construction.
    constructor() {}
    //
    // Public methods.
    public get(): IYMLConfiguration {
        return this.config.getValue();
    }
    public getRebuildSH(): string {
        return this.rebuildSh.getValue();
    }
    public getYml(): string {
        return this.yml.getValue();
    }
    public reset(): void {
        this.config.next({
            indent: '  ',

            image: 'registry.gitlab.com/daemonraco-dev/vscoder:latest',
            restart: 'unless-stopped',
            security: undefined,

            directories: {
                cache: './cache',
                code: './code',
                ssh: `./cache/ssh`,
                sshSeparated: false,
            },
            ports: {
                ssh: 2222,
                monitor: 9999,
                webCoder: 8888,
            },
            bundles: {
                angular: false,
                c: false,
                docker: false,
                go: false,
                nestjs: false,
                node: '',
                php: '',
                react: false,
                rust: false,
                vue: false,
            },
            features: {
                webCoder: false,
            },
        });

        this.build();
    }
    public set(newConf: IYMLConfiguration): void {
        this.config.next({ ...this.config.getValue(), ...newConf });
        this.build();
    }
    //
    // Protected methods.
    protected build(): void {
        this.buildRebuildSh();
        this.buildYml();
    }
    protected buildRebuildSh(): void {
        const config: IYMLConfiguration = this.get();

        let script: string = `#!/bin/bash\n`;
        script += `. /usr/share/dev-helper/dev-helper.constants.sh;\n`;
        script += `#\n`;

        config.features?.webCoder && (script += `dev-helper enable-web-coder;\n`);

        config.bundles?.c && (script += `dev-helper install-c;\n`);
        config.bundles?.docker && (script += `dev-helper install-docker;\n`);
        config.bundles?.go && (script += `dev-helper install-go;\n`);
        config.bundles?.rust && (script += `dev-helper install-rust;\n`);

        config.bundles?.php === 'apache' && (script += `dev-helper install-php-apache;\n`);
        config.bundles?.php === 'nginx' && (script += `dev-helper install-php-nginx;\n`);

        config.bundles?.node === '16' && (script += `dev-helper install-node-16;\n`);
        config.bundles?.node === '18' && (script += `dev-helper install-node-18;\n`);
        config.bundles?.node === '19' && (script += `dev-helper install-node-19;\n`);
        config.bundles?.node === '20' && (script += `dev-helper install-node-20;\n`);
        config.bundles?.node === '21' && (script += `dev-helper install-node-21;\n`);

        config.bundles?.angular && (script += `dev-helper install-angular;\n`);
        config.bundles?.nestjs && (script += `dev-helper install-nestjs;\n`);
        config.bundles?.react && (script += `dev-helper install-react;\n`);
        config.bundles?.vue && (script += `dev-helper install-vue;\n`);

        this.rebuildSh.next(script);
    }
    protected buildYml(): void {
        const config: IYMLConfiguration = this.get();

        let yml: string = `version: '3.5'\n`;
        yml += `\n`;

        yml += `services:\n`;
        yml += `${config.indent}developer:\n`;
        yml += `${config.indent?.repeat(2)}image: ${config.image}\n`;
        if (config.restart) {
            yml += `${config.indent?.repeat(2)}restart: ${config.restart}\n`;
        }
        if (config.security) {
            yml += `${config.indent?.repeat(2)}environment:\n`;
            yml += `${config.indent?.repeat(4)}- VSCODER_SECURITY=${config.security}\n`;
        }
        //
        // Ports.
        yml += `\n`;
        yml += `${config.indent?.repeat(2)}ports:\n`;
        yml += `${config.indent?.repeat(3)}- "${config.ports?.ssh}:22" # SSH Port.\n`;
        yml += `${config.indent?.repeat(3)}- "${config.ports?.monitor}:9999" # Monitor Port.\n`;
        if (config.features?.webCoder) {
            yml += `${config.indent?.repeat(3)}- "${config.ports?.webCoder}:8888" # WebCoder Port.\n`;
        }
        //
        // Volumes
        yml += `\n`;
        yml += `${config.indent?.repeat(2)}volumes:\n`;
        yml += `${config.indent?.repeat(3)}- ${config.directories?.code}:/home/dev/code # Default codes directory.\n`;
        if (config.bundles?.php) {
            yml += `${config.indent?.repeat(3)}- ${config.directories?.code}-www:/home/dev/www # PHP Public codes.\n`;
        }
        yml += `\n`;
        if (config.bundles?.rust) {
            yml += `${config.indent?.repeat(3)}- ${config.directories?.cache}/cargo:/home/dev/.cargo # Rust assets.\n`;
        }
        yml += `${config.indent?.repeat(3)}- ${config.directories?.cache}/dev-helper:/home/dev/.dev-helper\n`;
        if (config.bundles?.go) {
            yml += `${config.indent?.repeat(3)}- ${config.directories?.cache}/go:/home/dev/go # Golang assets.\n`;
        }
        yml += `${config.indent?.repeat(3)}- ${config.directories?.cache}/memory:/dev-helper/memory\n`;
        yml += `${config.indent?.repeat(3)}- ${config.directories?.ssh}:/home/dev/.ssh\n`;
        yml += `${config.indent?.repeat(3)}- ${config.directories?.cache}/vscode:/home/dev/.vscode-server\n`;
        if (config.bundles?.docker) {
            yml += `\n`;
            yml += `${config.indent?.repeat(
                3,
            )}- /var/run/docker.sock:/var/run/docker.sock # Connecting host's Docker socket.\n`;
        }

        this.yml.next(yml);
    }
}
