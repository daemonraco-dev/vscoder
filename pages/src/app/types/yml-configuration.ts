export enum IYMLConfigurationSecurity {
    Insecure = 'insecure',
    Password = 'password',
    Secure = 'secure',
}

export interface IYMLConfiguration {
    bundles?: IYMLConfigurationBundles;
    directories?: IYMLConfigurationDirectories;
    features?: IYMLConfigurationFeatures;
    image?: string;
    indent?: string;
    ports?: IYMLConfigurationPorts;
    restart?: string;
    security?: IYMLConfigurationSecurity;
}

export interface IYMLConfigurationBundles {
    angular: boolean;
    c: boolean;
    docker: boolean;
    go: boolean;
    nestjs: boolean;
    node: string;
    php: string;
    react: boolean;
    rust: boolean;
    vue: boolean;
}

export interface IYMLConfigurationDirectories {
    cache: string;
    code: string;
    ssh: string;
    sshSeparated: boolean;
}

export interface IYMLConfigurationFeatures {
    webCoder: boolean;
}

export interface IYMLConfigurationPorts {
    monitor: number;
    ssh: number;
    webCoder: number;
}
