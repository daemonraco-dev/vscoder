import { BrowserModule } from '@angular/platform-browser';
import { HighlightModule, HIGHLIGHT_OPTIONS } from 'ngx-highlightjs';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { PagesModule } from './pages';
import { SharedModule } from './shared';

import { AppComponent } from './app.component';

@NgModule({
    declarations: [
        AppComponent,
    ],
    imports: [
        AppRoutingModule,
        BrowserModule,
        HighlightModule,
        PagesModule,
        SharedModule,
    ],
    providers: [{
        provide: HIGHLIGHT_OPTIONS,
        useValue: {
            coreLibraryLoader: () => import('highlight.js/lib/core'),
            languages: {
                shell: () => import('highlight.js/lib/languages/shell'),
                yaml: () => import('highlight.js/lib/languages/yaml'),
            },
        },
    }],
    bootstrap: [AppComponent],
})
export class AppModule { }
