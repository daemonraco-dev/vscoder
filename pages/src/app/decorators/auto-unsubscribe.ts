export function AutoUnsubscribe(blackList: string[] = []) {
    return function (constructor: any) {
        const original: Function = constructor.prototype.ngOnDestroy;

        constructor.prototype.ngOnDestroy = function () {
            for (let prop in this) {
                if (!blackList.includes(prop)) {
                    if (this[prop] && typeof this[prop].unsubscribe === 'function') {
                        this[prop].unsubscribe();
                        this[prop] = null;
                    }
                }
            }
            original && typeof original === 'function' && original.apply(this, arguments);
        };
    }
}
