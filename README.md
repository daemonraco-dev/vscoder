# VSCoder

## What is this?

This is a small docker image that allow you to connect into it with VSCode and
start developing in an isolated ubuntu-like environment.

## How to use it

Our recomended way to use it is through _docker-compose_ with a configuration like
this one (visit the [online builder](https://daemonraco-dev.gitlab.io/vscoder) for
an easier configuration experience):

```yml
version: '3'

services:
    developer:
        image: registry.gitlab.com/daemonraco-dev/vscoder:latest
        ports:
            - '2222:22' # SSH access.
            - '9999:9999' # Status Web Monitor.
        environment:
            - VSCODER_SECURITY=insecure
        volumes:
            - /path/to/my/code:/home/dev/code
            - cache-dev-helper:/home/dev/.dev-helper
            - cache-memory:/dev-helper/memory
            - cache-ssh:/home/dev/.ssh
            - cache-vscode:/home/dev/.vscode-server

volumes:
    cache-dev-helper:
    cache-memory:
    cache-ssh:
    cache-vscode:
```

After you create this `docker-compose.yml`, you can launch it with
`docker-compose up -d` and access it remotely with VSCode though port `2222` and
start developing in an isolated environment.

If you rather have more control over your volumes, you can take this example:

```yml
version: '3'

services:
    developer:
        image: registry.gitlab.com/daemonraco-dev/vscoder:latest
        ports:
            - '2222:22' # SSH access.
            - '9999:9999' # Status Web Monitor.
        environment:
            - VSCODER_SECURITY=insecure
        volumes:
            - /path/to/my/code:/home/dev/code
            - ./cache/dev- helper:/home/dev/.dev-helper
            - ./cache/memory:/dev-helper/memory
            - ./cache/ssh:/home/dev/.ssh
            - ./cache/vscode:/home/dev/.vscode-server
```

### Volumes

This image has many folders/volumes that may be of interest:

-   `/home/dev/.ssh`: Mounting this folder allows you to cache SSH credentials so
    you don't need to re-register yourself with `ssh-copy-id` for example.
-   `/home/dev/.vscode-server`: This will let you keep all you VSCode
    configurations so you don't need to redo them and install all extensions
    again.
-   `/dev-helper/memory`: This image has a mechanism to keep track of what you
    install, so, if you download a new version of it, you may need to run those
    installation commands again, unless you mount this one.
-   `/home/dev/.dev-helper`: Here you'll have several files and directories that
    may not be mountable but are useful to have cached, files like `~/.gitconfig`,
    _bash_ history and those things.

## Credentials

_VSCoder_ gives you a default user called `dev` and it's the one you should use
because many internal behaviors install having it as default.

If you use the _docker-compose_ example above and set the environment variable
`VSCODER_SECURITY` to `insecure`, your environment will boot with the password
`dev`.

Nonetheless, we recommend you read [this](docs/security.md) for more security
information.

## VSCode Remotely

If you don't know how to access environments remotely with VSCode, just visit the
extension
[`ms-vscode-remote.vscode-remote-extensionpack`](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack)
and you'll find instruction on how to install and use it.

## No Local VSCode

If you don't want to install VSCode in you computer to later access you container,
there's an alternative, just read the section
[web-coder](docs/dev-helper.md#web-coder) in the `dev-helper` documentation.

## Dev Helper

The isolated environment comes with a few handy commands that can quickly set you
up with different tools for different needs.

Run `dev-helper` to get more information on available options.

You may also visit [this link](docs/dev-helper.md) to read information about it.

## Known Issues

There are a set of known and expected issues you may find while using this image
and here you may find some information about them:

-   [Known Issues](docs/known-issues.md)

## Extend VSCoder

If you like this image but want to add more on it, there's a way, just visit
[this link](docs/extend.md).

## License

This image is distributed under the MIT license and the author is Alejandro Dario
Simi. You can read the license disclaimer [here](LICENSE).
