FROM ubuntu:latest
#
# Installing common packages.
RUN apt-get update && apt-get install sudo openssh-server xauth -y
RUN apt-get install curl wget vim bash-completion net-tools git tmux tmuxp tmux-plugin-manager -y
#
# Creating the developer user.
RUN useradd -rm -d /home/dev -s /bin/bash -g root -G sudo -u 1000 dev
RUN echo 'dev:dev' | chpasswd
#
# Disabling password requests when using 'sudo'.
RUN echo 'dev ALL=(ALL) NOPASSWD:ALL' > /etc/sudoers.d/dev-nopassword
#
# Configuring SSH X11 forwading.
RUN sed -i 's/^[#]*X11Forwarding.*$/X11Forwarding yes/g' /etc/ssh/sshd_config
RUN sed -i 's/^[#]*X11DisplayOffset.*$/X11DisplayOffset 10/g' /etc/ssh/sshd_config
RUN sed -i 's/^[#]*X11UseLocalhost.*$/X11UseLocalhost no/g' /etc/ssh/sshd_config
#
# Installing the SSH server.
RUN service ssh start
#
# Installing and activating the helper.
COPY ./scripts/dev-helper /usr/bin/dev-helper
RUN chmod a+x /usr/bin/dev-helper
#
# Adding helper tools.
COPY ./scripts/dev-helper.*.sh /usr/share/dev-helper/
COPY ./scripts/install-*.sh /usr/share/dev-helper/
COPY ./scripts/feature.*.sh /usr/share/dev-helper/
RUN ln -s /usr/share/dev-helper/dev-helper.autocomplete.sh /usr/share/bash-completion/completions/dev-helper
#
# Replacing login welcome message.
# RUN mv -v /etc/update-motd.d/00-header /etc/update-motd.d/05-header
RUN rm -fv /etc/update-motd.d/*
COPY ./scripts/login-messages/welcome-message /etc/update-motd.d/00-welcome-dev-helper
COPY ./scripts/login-messages/rebuilding-message /etc/update-motd.d/01-rebuilding-dev-helper
COPY ./scripts/login-messages/and-now-message /etc/update-motd.d/04-and-now-dev-helper
RUN chmod -v a+x /etc/update-motd.d/*-dev-helper
#
# Adding bash tools.
COPY ./scripts/bash-tools /usr/share/dev-helper/bash-tools
#
# Some documentation.
COPY ./LICENSE /usr/share/doc/dev-helper/copyright
#
# Exposing SSH server.
EXPOSE 22
#
# Installing DevHelperUI and setting it up as a service.
RUN mkdir -vp /dev-helper/ui /dev-helper/ui/docs
COPY ./helper-ui/config.json /dev-helper/ui/config.json
COPY ./helper-ui/dev-helper-ui /dev-helper/ui/dev-helper-ui
COPY ./helper-ui/assets /dev-helper/ui/assets
COPY ./README.md /dev-helper/ui/docs/README.md
COPY ./docs /dev-helper/ui/docs/docs
COPY ./helper-ui/dev-helper-ui.service /etc/init.d/dev-helper-ui
RUN update-rc.d dev-helper-ui defaults
EXPOSE 9999
#
# Adding web coder tools
COPY ./scripts/web-coder /usr/share/dev-helper/web-coder
EXPOSE 8888
#
# Health Checker.
COPY ./scripts/health-checker.sh /usr/share/dev-helper/health-checker.sh
HEALTHCHECK --interval=30s --timeout=30s --start-period=5s --retries=3 CMD ["/bin/bash", "/usr/share/dev-helper/health-checker.sh"]
#
# Setting up the entrypoint
COPY ./scripts/entrypoint.sh /dev-helper/entrypoint.sh
ENTRYPOINT ["/bin/bash", "/dev-helper/entrypoint.sh"]
#
# Default command.
CMD ["/usr/sbin/sshd", "-D"]
